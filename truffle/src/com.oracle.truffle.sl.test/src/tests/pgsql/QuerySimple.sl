function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, From(emp));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
