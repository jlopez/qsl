function main()
{
  minsalary = 2500;
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  dept = table("dept", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.empno = x.empno; res.ename = x.ename;
                     res.deptname = fun(deptno){ return next(
		      Select(fun(y){ res2 = new(); res2.loc = y.loc; return res2; },
		      Where(fun(y){ y.deptno == deptno; },
		      From(dept)))).loc; }(x.deptno); return res; },
      Where(fun(x){ return x.sal >= minsalary; },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.empno + ", " + tuple.ename + ", " + tuple.deptname);
  }
}
