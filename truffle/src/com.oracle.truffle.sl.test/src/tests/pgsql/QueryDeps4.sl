function f(x)
{
  o = new();
  o.ename = x.ename;
  return o;
}

function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  g = f;
  o = Select(fun(x){ return g(x); }, From(emp));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
