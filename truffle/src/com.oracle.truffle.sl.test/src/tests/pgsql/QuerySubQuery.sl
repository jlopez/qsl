function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  dept = table("dept", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; },
      Where(fun(x){ return x.deptno == Select(fun(x){ return x.deptno; },
      		    	   	       Where(fun(x){ return x.loc == "NEW YORK"; },
				       From(dept))); },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
