function main () {
	movie = table("movie", "PostgreSQL", "postgre.config", "public");
	result = Where(fun (r) { return r.year > 2010; },
			     From(movie));

	while ((line = next(result)) != null) {
		println(line.title + " (" + line.year + ")");
	}
}
