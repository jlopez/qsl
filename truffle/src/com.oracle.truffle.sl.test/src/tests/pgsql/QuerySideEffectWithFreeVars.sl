function conv(change, x)
{
  return x * change / 100;
}

function main()
{
  minsalary = 2500;
  change = 89;
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.empno = x.empno; res.ename = x.ename;
                     res.salary = fun(dol, dept){ res = conv(change, dol);
		     while (res > 500 * dept) { res = conv(change, res); } return res; }(x.sal, x.deptno); return res; },
      Where(fun(x){ return x.sal >= minsalary; },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.empno + ", " + tuple.ename + ", " + tuple.salary);
  }
}
