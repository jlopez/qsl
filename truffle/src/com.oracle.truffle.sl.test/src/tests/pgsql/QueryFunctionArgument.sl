function dolToEuro(x)
{
  return x * 89 / 100;
}

function dolToYen(x)
{
  return x * 12392 / 100;
}

function query(converter)
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  return Select(fun(x){ res = new(); res.salary = converter(x.sal); return res; },
  	 From(emp));
}

function main()
{
  o = query(dolToEuro);
  while ((tuple = next(o)) != null)
  {
    println(tuple.salary);
  }
}
