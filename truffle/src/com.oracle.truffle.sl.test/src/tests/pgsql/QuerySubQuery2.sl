// Returns the names of employees earning at least minSalary in the curr
// currency
function atLeast(minSalary, curr)
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  change = table("change", "PostgreSQL", "postgre.config", "public");
  if (curr == "USD") {
    return Select(fun(e) { r = new(); r.name = e.ename; return r; },
           Where(fun(e) { return e.sal >= minSalary; },
           From(emp)));
  }
  return Select(fun(e) { r = new(); r.name = e.ename; return r; },
         Where(fun(e) { return e.sal >= minSalary *
               Select(fun(e) { r = new(); r.change = e.change; return r; },
               Where(fun(r) { return r.cfrom == "USD" && r.cto == curr; },
               From(change))); },
         From(emp)));
}

function main()
{
  richUSPeople = atLeast(2000, "USD");
  richEURPeople = atLeast(2000, "EUR");

  while ((tuple = next(richUSPeople)) != null)
  {
    println(tuple.name);
  }
  println("----");
  while ((tuple = next(richEURPeople)) != null)
  {
    println(tuple.name);
  }
}
