function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, Where(fun(x){ return x.sal * 2 >= 2547; },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
