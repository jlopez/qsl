function g(x)
{
  return f(x);
}

function f(x)
{
  o = new();
  o.ename = x.ename;
  return o;
}

function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(g, From(emp));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
