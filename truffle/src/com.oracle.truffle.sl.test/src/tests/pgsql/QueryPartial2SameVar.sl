function selectEmpnoAndEname(query)
{
  return Select(fun(x){ res = new(); res.empno = x.empno; res.ename = x.ename; return res; }, query);
}

function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = From(emp);
  o = selectEmpnoAndEname(o);
  while ((tuple = next(o)) != null)
  {
    println(tuple.empno + ", " + tuple.ename);
  }
}
