function DOUBLE(x)
{
  return x * 2;
}

function COMP(x, y)
{
  return DOUBLE(x) + y;
}

function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; },
      Where(fun(x){ return COMP(x.sal + 9, x.deptno) >= COMP(x.sal, 20); },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
