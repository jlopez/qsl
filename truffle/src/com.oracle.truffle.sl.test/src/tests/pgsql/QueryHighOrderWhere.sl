function runtimeGreater (r,runtime) {
	return r.runtime > runtime;
}

function yearAfter (r, year) {
	return r.year > year;
}

function rankBelow (r, rank) {
	return r.rank < rank;
}

function makeCond (bits) {
	pred0 = fun (r) { return true; };
	pred1 = null;
	pred2 = null;
	pred3 = null;

	if ((bits % 2) == 1) {
		pred1 = (fun (r) { return rankBelow(r,10); });
	} else {
		pred1 = pred0;
	}

	bits = bits / 2;

	if ((bits % 2) == 1) {
		pred2 = (fun (r) { return pred1 (r) && runtimeGreater(r,80); });
	} else {
		pred2 = pred1;
	}

	bits = bits / 2;

	if ((bits % 2) == 1) {
		pred3 = (fun (r) { return  pred2 (r) && yearAfter (r, 2000); });
	} else {
		pred3 = pred2;
	}

	return pred3;
}

function main () {
	movie = table("movie", "PostgreSQL", "postgre.config", "public");

	p = makeCond (7);

	result = Where(p,From(movie));

	while ((line = next(result)) != null) {
		println(line.title + " (" + line.year + ")");
	}
}
