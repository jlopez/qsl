function f(x)
{
  o = new();
  o.ename = x.ename;
  return o;
}

function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = Select(fun(x){ return f(x); }, From(emp));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
