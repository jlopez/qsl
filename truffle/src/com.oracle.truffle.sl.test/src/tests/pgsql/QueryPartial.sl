function main()
{
  emp = table("emp", "PostgreSQL", "postgre.config", "public");
  o = From(emp);
  o2 = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, o);
  while ((tuple = next(o2)) != null)
  {
    println(tuple.ename);
  }
}
