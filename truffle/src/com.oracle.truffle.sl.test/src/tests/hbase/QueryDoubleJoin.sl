function moviesAfter(m, year) {
	return Where(fun (r) { return r.year >= year; }, m);
}

function peopleNamed (p, name) {
	return Where (fun (u) { return u.lastname == name; }, p);
}

function main () {
	movie = table("movie", "HBase", "hbase-site.xml", "default");
	people = table("people", "HBase", "hbase-site.xml", "default");
	role = table("role", "HBase", "hbase-site.xml", "default");

	result =
	Select (fun (x) { o = new ();  o.title = x.title; o.name = x.name; return o; },
	Join(moviesAfter(From(movie), 2000),
	Join(peopleNamed(From(people), "Eastwood"),
	From(role),
	fun (r, p) { return r.pid == p.pid; }),
	fun (m, rp) { return m.mid == rp.mid; })
	);
	while ((row = next(result)) != null) {
		println(row.title + " (" + row.name + ")");
	}
}
