// Returns the exchange rate between rfrom and rto
function getRate(rfrom, rto)
{
  change = table("change", "Hive", "hive.config", "main");
  rate = next(Where(fun(r) { return r.cfrom == rfrom && r.cto == rto; },
              From(change)));
  if (rfrom == rto) {
    return 1;
  }
  else {
    return rate.change;
  }
}

// Returns the names of employees earning at least minSalary in the curr
// currency
function atLeast(minSalary, curr)
{
  emp = table("emp", "Hive", "hive.config", "main");
  return Select(fun(e) { r = new(); r.name = e.ename; return r; },
         Where(fun(e) { return e.sal >= minSalary * getRate("USD", curr); },
         From(emp)));
}

function main()
{
  richUSPeople = atLeast(2000, "USD");
  richEURPeople = atLeast(2000, "EUR");

  while ((tuple = next(richUSPeople)) != null)
  {
    println(tuple.name);
  }
  println("----");
  while ((tuple = next(richEURPeople)) != null)
  {
    println(tuple.name);
  }
}
