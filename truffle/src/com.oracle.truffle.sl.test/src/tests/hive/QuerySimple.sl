function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, From(emp));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
