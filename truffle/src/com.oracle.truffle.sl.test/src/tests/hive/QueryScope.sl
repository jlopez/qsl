// Query should be: select * from emp where deptno < 3
function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  y = 2; // l1 <- 2
  o = Where(fun(x){ return x.deptno < y; }, From(emp)); // y is bound to l1
  y = 3; // l1 <- 3
  fun(y) { // l2 <- 1
    while ((tuple = next(o)) != null)
    {
      println(tuple.ename);
    }
  } (1);
}
