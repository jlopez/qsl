function main()
{
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, From(table("emp", "Hive", "hive.config", "main")));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
