function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  dept = table("dept", "Hive", "hive.config", "main");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; },
      Where(fun(x){ return x.loc == "NEW YORK"; },
      Join(From(emp), From(dept), fun(x,y) { return x.deptno == y.deptno; })));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
