function main()
{
  minsalary = 2500;
  emp = table("emp", "Hive", "hive.config", "main");
  o = Select(fun(x){ res = new(); res.empno = x.empno; res.ename = x.ename;
                     res.salary = fun(dol){ return dol * 89 / 100; }(x.sal); return res; },
      Where(fun(x){ return x.sal >= minsalary || x.mgr + minsalary > 10300; },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.empno + ", " + tuple.ename + ", " + tuple.salary);
  }
}
