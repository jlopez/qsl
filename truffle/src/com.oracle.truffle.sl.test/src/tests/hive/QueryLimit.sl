function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  o = Limit(10, Select(fun(x){ res = new(); res.ename = x.ename; return res; }, From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
