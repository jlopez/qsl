function selectEname(query)
{
  x = fun(x){ res = new(); res.ename = x.ename; return res; };
  return Select(x, query);
}

function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  o = From(emp);
  o = selectEname(o);
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
