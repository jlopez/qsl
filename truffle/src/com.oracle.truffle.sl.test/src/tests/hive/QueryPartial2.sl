function selectEmpnoAndEname(query)
{
  return Select(fun(x){ res = new(); res.empno = x.empno; res.ename = x.ename; return res; }, query);
}

function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  o = From(emp);
  o2 = selectEmpnoAndEname(o);
  while ((tuple = next(o2)) != null)
  {
    println(tuple.empno + ", " + tuple.ename);
  }
}
