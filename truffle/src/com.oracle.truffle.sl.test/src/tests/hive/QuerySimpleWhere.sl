function main () {
	movie = table("movie", "Hive", "hive.config", "main");
	result = Where(fun (r) { return r.year > 2010; },
			     From(movie));

	while ((line = next(result)) != null) {
		println(line.title + " (" + line.year + ")");
	}
}
