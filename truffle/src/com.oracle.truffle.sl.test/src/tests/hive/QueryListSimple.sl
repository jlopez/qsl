function main()
{
  t1 = new();
  t1.name = "John";
  t2 = new();
  t2.name = "Jim";
  o = Select(fun(x){ res = new(); res.ename = x.name; return res; }, [t1, t2]);
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
