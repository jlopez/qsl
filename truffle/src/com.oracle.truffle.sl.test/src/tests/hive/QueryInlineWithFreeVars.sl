function main()
{
  tableName = "emp";
  dbName = "PostgreSQL";
  configFile = "postgre.config";
  schemaName = "public";
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; }, From(table(tableName, dbName, configFile, schemaName)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
