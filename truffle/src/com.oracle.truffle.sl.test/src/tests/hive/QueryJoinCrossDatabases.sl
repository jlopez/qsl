function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  dept = table("dept", "PostgreSQL", "postgre2.config", "public");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; },
      Where(fun(x){ return x.loc == "NEW YORK"; },
      Join(From(emp), From(dept), fun(x, y){return x.deptno == y.deptno;})));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
