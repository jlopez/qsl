function main () {
	movie = table("movie",          // table name
								"PostgreSQL",     // driver
								"postgre.config", // connection parameters
								"public");        // schema (namespace)

	result = From(movie);	// load everything

	while ((line = next(result)) != null) {
		println(line.title);
	}
}
