function main()
{
  emp = table("emp", "Hive", "hive.config", "main");
  dept = table("dept", "Hive", "hive.config", "main");
  o = Select(fun(x){ res = new(); res.ename = x.ename; return res; },
      Where(fun(x){ return x.deptno == Select(fun(x){ return x.deptno; },
      		    	   	       Where(fun(x){ return x.loc == "NEW YORK"; },
				       From(dept))); },
      From(emp)));
  while ((tuple = next(o)) != null)
  {
    println(tuple.ename);
  }
}
