package com.oracle.truffle.sl.external;

import java.io.IOException;

import org.apache.hadoop.hive.ql.exec.UDF;

import com.oracle.truffle.sl.SLLanguage;

public class SLExecute extends UDF {
    public static String evaluate(final String program) {
        try {
            return SLLanguage.executeSL(program).getValue().toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
