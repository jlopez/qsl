package com.oracle.truffle.sl.external;

import org.apache.hadoop.hive.ql.exec.UDF;

import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.runtime.SLValue;

public class SLTranslateBackToBoolean extends UDF {
    public static Boolean evaluate(final SLValue v) {
        return SLLanguage.translateBackToBoolean(v);
    }
}
