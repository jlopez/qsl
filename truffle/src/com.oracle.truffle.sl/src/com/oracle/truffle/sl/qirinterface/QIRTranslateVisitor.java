package com.oracle.truffle.sl.qirinterface;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotKind;
import com.oracle.truffle.api.frame.FrameSlotTypeException;
import com.oracle.truffle.api.frame.MaterializedFrame;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.sl.SLException;
import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.builtins.*;
import com.oracle.truffle.sl.nodes.*;
import com.oracle.truffle.sl.nodes.access.*;
import com.oracle.truffle.sl.nodes.call.SLInvokeNode;
import com.oracle.truffle.sl.nodes.controlflow.*;
import com.oracle.truffle.sl.nodes.expression.*;
import com.oracle.truffle.sl.nodes.local.*;
import com.oracle.truffle.sl.nodes.query.*;
import com.oracle.truffle.sl.runtime.SLContext;
import com.oracle.truffle.sl.runtime.SLFunction;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.util.QIRException;
import qir.ast.expression.*;

/**
 * This visitor translates a {@link SLStatementNode} into a {@link QIRNode}.
 */
public final class QIRTranslateVisitor implements SLNodeVisitor<QIRNode> {
    private final SLLanguage language;
    private final MaterializedFrame frame;

    public QIRTranslateVisitor(final SLLanguage language, final MaterializedFrame frame) {
        this.language = language;
        this.frame = frame;
    }

    public QIRTranslateVisitor(final SLLanguage language) {
        this(language, null);
    }

    private final SourceSection dummy = null;

    @Override
    public final QIRNode visit(final SLFunctionBodyNode fBody) {
        return fBody.getBodyNode().accept(this);
    }

    @Override
    public final QIRNode visit(final SLReturnNode ret) {
        return ret.getValueNode().accept(this);
    }

    @Override
    public final QIRNode visit(final SLIfNode ifNode) {
        return new QIRIf(ifNode.getSourceSection(), ifNode.getConditionNode().accept(this), ifNode.getThenPartNode().accept(this), ifNode.getElsePartNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLWhileNode whileNode) {
        return whileNode.accept(new IsExportableVisitor(language, frame))
                        ? new QIRExportableTruffleNode(whileNode.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, whileNode.getSourceSection().getCharacters().toString())
                        : new QIRUnexportableTruffleNode(whileNode.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, whileNode.getSourceSection().getCharacters().toString());
    }

    /**
     * Translation of a sequence of statements. Note: This is also where the STRAD-ASSIGN rules are
     * handled.
     */
    @Override
    public final QIRNode visit(final SLBlockNode block) {
        QIRNode result = null;
        final SLStatementNode[] children = block.getBodyNodes();
        final int len = children.length;

        if (len == 0) // Block is empty
            return QIRNull.getInstance();

        result = children[len - 1].accept(this); // The last statement of the block
        /*
         * In this loop, we wrap the result with the other statements of the block in reverse order
         * in the way described by the STRAD-ASSIGN rules.
         */
        for (int i = len - 2; i > -1; i--) {
            final SLStatementNode child = children[i];
            if (child instanceof SLWriteLocalVariableNode) { // STRAD-ASSIGN-ID
                final FrameSlot slot = ((SLWriteLocalVariableNode) child).getSlot();
                final String var = (String) slot.getIdentifier();
                final SLExpressionNode value = ((SLWriteLocalVariableNode) child).getValueNode();

                // If the assignment value is a SLReadArgumentNode, then we translate to a lambda.
                result = new QIRLambda(dummy, null, new QIRVariable(dummy, var, slot), result, new FrameDescriptor());
                // Else we apply STRAD-ASSIGN-ID normally
                if (!(value instanceof SLReadArgumentNode))
                    result = new QIRApply(dummy, result, value.accept(this));
            } else if (child instanceof SLWritePropertyNode) { // STRAD-ASSIGN-FIELD and
                                                               // STRAD-ASSIGN-DOT
                // TODO: Take care of STRAD-ASSIGN-DOT
                final SLReadLocalVariableNode receiver = (SLReadLocalVariableNode) ((SLWritePropertyNode) child).getReceiverNode();
                final SLExpressionNode value = ((SLWritePropertyNode) child).getValueNode();
                final FrameSlot slot = receiver.getSlot();
                result = new QIRApply(dummy, new QIRLambda(dummy, null, new QIRVariable(dummy, (String) slot.getIdentifier(), slot), result, new FrameDescriptor()),
                                new QIRRcons(dummy, ((SLStringLiteralNode) ((SLWritePropertyNode) child).getNameNode()).getValue(), value.accept(this), receiver.accept(this)));
            } else
                throw new UnsupportedOperationException("Cannot translate \"" + block.getSourceSection() + "\" to QIR: untranslatable sequence.");
        }
        return result;
    }

    @Override
    public final QIRNode visit(final SLParenExpressionNode e) {
        return e.getExpression().accept(this);
    }

    @Override
    public final QIRNode visit(final SLInvokeNode call) {
        final QIRNode fun = call.getFunctionNode().accept(this);
        QIRNode res = fun;

        if (call.getArgumentNodes().length == 0) // call is an application with no arguments
            return new QIRApply(dummy, fun, null);
        for (final SLExpressionNode arg : call.getArgumentNodes())
            res = new QIRApply(dummy, res, arg.accept(this));
        return res;
    }

    @Override
    public final QIRNode visit(final SLFunctionLiteralNode fun) {
        if (fun.getFunctionName().charAt(0) == '#') {
            final SLContext context = language.getContextReference().get();
            final SLFunction f = context.getFunctionRegistry().lookup(fun.getFunctionName(), false);
            return QIRInterface.SLToQIRType.apply(context).apply(f.getCallTarget().getRootNode().getSourceSection(), f);
        }
        return new QIRVariable(fun.getSourceSection(), fun.getFunctionName());
    }

    @Override
    public final QIRNode visit(final SLReadArgumentNode var) {
        // Should be handled elsewhere like in SLBlockNode.
        throw new SLException("Error in translation to QIR: should not have visited a ReadArgumentNode.", var);
    }

    @Override
    public final QIRNode visit(final SLReadLocalVariableNode var) {
        final FrameSlot slot = var.getSlot();

        if (frame != null && frame.getFrameDescriptor().getFrameSlotKind(slot) != FrameSlotKind.Illegal)
            try {
                return QIRInterface.SLToQIRType.apply(language.getContextReference().get()).apply(var.getSourceSection(), frame.getObject(var.getSlot()));
            } catch (FrameSlotTypeException e) {
                e.printStackTrace();
                throw new QIRException("Internal environment error");
            } catch (IllegalArgumentException e) {
            }
        return new QIRVariable(var.getSourceSection(), var.getSourceSection().getCharacters().toString(), var.getSlot());
    }

    @Override
    public final QIRNode visit(final SLWriteLocalVariableNode var) {
        // STRAD-ASSIGN-ID should be handled in SLBlockNode.
        throw new SLException("Error in translation to QIR: should not have visited an assignment.", var);
    }

    @Override
    public final QIRNode visit(final SLReadPropertyNode dot) {
        return new QIRRdestr(dot.getSourceSection(), dot.getReceiverNode().accept(this), ((SLStringLiteralNode) dot.getNameNode()).getValue());
    }

    @Override
    public final QIRNode visit(final SLWritePropertyNode writeDot) {
        // STRAD-ASSIGN-FIELD and STRAD-ASSIGN-DOT should be handled in SLBlockNode.
        throw new SLException("Error in translation to QIR: should not have visited an assignment.", writeDot);
    }

    @Override
    public final QIRNode visit(final SLListNode list) {
        QIRList res = QIRLnil.getInstance();
        final Deque<QIRNode> elements = new ArrayDeque<>();
        final SourceSection src = list.getSourceSection();

        Arrays.asList(list.elements).forEach(x -> elements.push(x.accept(this)));
        for (QIRNode e : elements)
            res = new QIRLcons(src, e, res);
        return res;
    }

    @Override
    public final QIRNode visit(final SLAddNode add) {
        return QIRPlusNodeGen.create(add.getSourceSection(), add.getLeftNode().accept(this), add.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLSubNode sub) {
        return QIRMinusNodeGen.create(sub.getSourceSection(), sub.getLeftNode().accept(this), sub.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLMulNode mul) {
        return QIRStarNodeGen.create(mul.getSourceSection(), mul.getLeftNode().accept(this), mul.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLDivNode div) {
        return QIRDivNodeGen.create(div.getSourceSection(), div.getLeftNode().accept(this), div.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLModNode mod) {
        return QIRModNodeGen.create(mod.getSourceSection(), mod.getLeftNode().accept(this), mod.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLEqualNode eq) {
        return QIREqualNodeGen.create(eq.getSourceSection(), eq.getLeftNode().accept(this), eq.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLogicalAndNode and) {
        return QIRAndNodeGen.create(and.getSourceSection(), and.getLeft().accept(this), and.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLogicalOrNode or) {
        return QIROrNodeGen.create(or.getSourceSection(), or.getLeft().accept(this), or.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLessOrEqualNode leq) {
        return QIRLowerOrEqualNodeGen.create(leq.getSourceSection(), leq.getLeftNode().accept(this), leq.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLessThanNode lt) {
        return QIRLowerThanNodeGen.create(lt.getSourceSection(), lt.getLeftNode().accept(this), lt.getRightNode().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLogicalNotNode not) {
        return QIRNotNodeGen.create(not.getSourceSection(), not.getValueNode().accept(this));
    }

    /**
     * Translation of a subquery.
     */
    @Override
    public final QIRNode visit(final SLQIRWrapperNode query) {
        return language.getContextReference().get().queries.get(query.id);
    }

    @Override
    public final QIRNode visit(final SLSelectNode select) {
        return new QIRProject(select.getSourceSection(), select.formatter.accept(this), select.query.accept(this));
    }

    @Override
    public final QIRNode visit(final SLFromNode from) {
        return new QIRScan(from.getSourceSection(), from.getTable().accept(this));
    }

    @Override
    public final QIRNode visit(final SLWhereNode where) {
        return new QIRFilter(where.getSourceSection(), where.getFilter().accept(this), where.getQuery().accept(this));
    }

    @Override
    public final QIRNode visit(final SLGroupNode group) {
        return new QIRGroupBy(group.getSourceSection(), group.getGroup().accept(this), group.getQuery().accept(this));
    }

    @Override
    public final QIRNode visit(final SLOrderNode order) {
        return new QIRSortBy(order.getSourceSection(), order.getOrder().accept(this), order.getIsAscending().accept(this), order.getQuery().accept(this));
    }

    @Override
    public final QIRNode visit(final SLJoinNode join) {
        return new QIRJoin(join.getSourceSection(), join.getFilter().accept(this), join.getLeft().accept(this), join.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLeftJoinNode join) {
        return new QIRLeftJoin(join.getSourceSection(), join.getFilter().accept(this), join.getLeft().accept(this), join.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final SLRightJoinNode join) {
        return new QIRRightJoin(join.getSourceSection(), join.getFilter().accept(this), join.getLeft().accept(this), join.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final SLLimitNode limit) {
        return new QIRLimit(limit.getSourceSection(), limit.getLimit().accept(this), limit.getQuery().accept(this));
    }

    @Override
    public final QIRNode visit(final SLBreakNode x) {
        // Should be handled in SLWhileNode.
        throw new SLException("Error in translation to QIR: should not have visited a break statement.", x);
    }

    @Override
    public final QIRNode visit(final SLDefineFunctionBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLEvalBuiltin x) {
        return new QIRUnexportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLGetSizeBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLHasSizeBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLHelloEqualsWorldBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLImportBuiltin x) {
        return new QIRUnexportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLIsExecutableBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLIsNullBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLNanoTimeBuiltin x) {
        return new QIRExportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    /**
     * In SL, we represent tuples (and lists) with objects.
     *
     * @param tnil Node representing {@code new} in SL.
     */
    @Override
    public final QIRNode visit(final SLNewObjectBuiltin tnil) {
        return new QIRLambda(tnil.getSourceSection(), "new", null, QIRRnil.getInstance(), new FrameDescriptor());
    }

    @Override
    public final QIRNode visit(final SLNextResQueryBuiltin next) {
        final QIRVariable list = new QIRVariable(dummy, "l");
        final QIRVariable tail = new QIRVariable(dummy, "tail");
        return new QIRLambda(next.getSourceSection(), "next", list,
                        new QIRLdestr(next.getSourceSection(), list, QIRNull.getInstance(),
                                        new QIRLambda(dummy, null, new QIRVariable(dummy, "head"), new QIRLambda(dummy, null, tail, tail, new FrameDescriptor()), new FrameDescriptor())),
                        new FrameDescriptor());
    }

    @Override
    public final QIRNode visit(final SLPrintlnBuiltin x) {
        return new QIRUnexportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLReadlnBuiltin x) {
        return new QIRUnexportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final SLStackTraceBuiltin x) {
        return new QIRUnexportableTruffleNode(x.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, x.getSourceSection().getCharacters().toString());
    }

    // TODO: This is not how to handle a table call properly
    @Override
    public final QIRNode visit(final SLTableBuiltin table) {
        final QIRVariable tableName = new QIRVariable(null, "__tmp__");
        final QIRVariable dbName = new QIRVariable(null, "__tmp2__");
        final QIRVariable configFile = new QIRVariable(null, "__tmp3__");
        final QIRVariable schemaName = new QIRVariable(null, "__tmp4__");
        return new QIRLambda(null, null, tableName,
                        new QIRLambda(null, null, dbName, new QIRLambda(null, null, configFile,
                                        new QIRLambda(null, null, schemaName, new QIRTable(table.getSourceSection(), tableName, dbName, configFile, schemaName),
                                                        new FrameDescriptor()),
                                        new FrameDescriptor()),
                                        new FrameDescriptor()),
                        new FrameDescriptor());
    }

    @Override
    public final QIRNode visit(final SLBooleanNode b) {
        return QIRBoolean.create(b.value);
    }

    @Override
    public final QIRNode visit(final SLStringLiteralNode s) {
        return new QIRString(s.getSourceSection(), s.getValue());
    }

    @Override
    public final QIRNode visit(final SLLongLiteralNode num) {
        return new QIRNumber(num.getSourceSection(), num.getValue());
    }

    @Override
    public final QIRNode visit(final SLDoubleLiteralNode num) {
        return new QIRDouble(num.getSourceSection(), num.getValue());
    }

    @Override
    public final QIRNode visit(final SLBigIntegerLiteralNode num) {
        return new QIRBigNumber(num.getSourceSection(), num.getValue().getValue());
    }

    @Override
    public final QIRNode visit(final SLNullNode s) {
        return QIRNull.getInstance();
    }

    @Override
    public final QIRNode visit(final SLUnboxNode x) {
        return x.getChild().accept(this);
    }

    @Override
    public final QIRNode visit(final SLStatementNode stmt) {
        throw new UnsupportedOperationException("Cannot translate to QIR: " + stmt.getSourceSection() + System.lineSeparator() + stmt.getClass().getName());
    }
}