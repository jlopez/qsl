/*
 * Copyright (c) 2014, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.sl.nodes.query;

import com.oracle.truffle.api.nodes.*;
import com.oracle.truffle.sl.nodes.SLExpressionNode;
import com.oracle.truffle.sl.nodes.SLNodeVisitor;

@NodeInfo(shortName = "OrderBy")
public final class SLOrderNode extends SLQueryNode {
    @Child private SLExpressionNode order;
    @Child private SLExpressionNode isAscending;
    @Child private SLExpressionNode query;

    public SLOrderNode(final SLExpressionNode order, final SLExpressionNode isAscending, final SLExpressionNode query) {
        this.order = order;
        this.isAscending = isAscending;
        this.query = query;
    }

    public final SLExpressionNode getOrder() {
        return order;
    }

    public final SLExpressionNode getIsAscending() {
        return isAscending;
    }

    public final SLExpressionNode getQuery() {
        return query;
    }

    @Override
    public <T> T accept(SLNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
