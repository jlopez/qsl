package com.oracle.truffle.sl.nodes;

import com.oracle.truffle.api.frame.VirtualFrame;

public class SLListNode extends SLExpressionNode {
    @Children public final SLExpressionNode[] elements;

    public SLListNode(final SLExpressionNode[] elements) {
        this.elements = elements;
    }

    @Override
    public Object[] executeGeneric(VirtualFrame frame) {
        Object[] res = new Object[elements.length];

        for (int i = 0; i < elements.length; i++)
            res[i] = elements[i];
        return res;
    }

    @Override
    public <T> T accept(SLNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
