/*
 * Copyright (c) 2012, 2018, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * The Universal Permissive License (UPL), Version 1.0
 *
 * Subject to the condition set forth below, permission is hereby granted to any
 * person obtaining a copy of this software, associated documentation and/or
 * data (collectively the "Software"), free of charge and under any and all
 * copyright rights in the Software, and any and all patent rights owned or
 * freely licensable by each licensor hereunder covering either (i) the
 * unmodified Software as contributed to or provided by such licensor, or (ii)
 * the Larger Works (as defined below), to deal in both
 *
 * (a) the Software, and
 *
 * (b) any piece of software and/or hardware listed in the lrgrwrks.txt file if
 * one is included with the Software each a "Larger Work" to which the Software
 * is contributed by such licensors),
 *
 * without restriction, including without limitation the rights to copy, create
 * derivative works of, display, perform, and distribute the Software and make,
 * use, sell, offer for sale, import, export, have made, and have sold the
 * Software and the Larger Work(s), and to sublicense the foregoing rights on
 * either these or other terms.
 *
 * This license is subject to the following condition:
 *
 * The above copyright notice and either this complete permission notice or at a
 * minimum a reference to the UPL must be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Checkstyle: stop
//@formatter:off
package com.oracle.truffle.sl.parser;

// DO NOT MODIFY - generated from SimpleLanguage.g4 using "mx create-sl-parser"

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.nodes.SLExpressionNode;
import com.oracle.truffle.sl.nodes.SLRootNode;
import com.oracle.truffle.sl.nodes.SLStatementNode;
import com.oracle.truffle.sl.parser.SLParseError;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings("all")
public class SimpleLanguageParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, WS=44, COMMENT=45, LINE_COMMENT=46, 
		IDENTIFIER=47, STRING_LITERAL=48, NUMERIC_LITERAL=49;
	public static final int
		RULE_simplelanguage = 0, RULE_function = 1, RULE_block = 2, RULE_statement = 3, 
		RULE_while_statement = 4, RULE_if_statement = 5, RULE_return_statement = 6, 
		RULE_expression = 7, RULE_logic_term = 8, RULE_logic_factor = 9, RULE_arithmetic = 10, 
		RULE_term = 11, RULE_factor = 12, RULE_lambda = 13, RULE_query = 14, RULE_member_expression = 15;
	public static final String[] ruleNames = {
		"simplelanguage", "function", "block", "statement", "while_statement", 
		"if_statement", "return_statement", "expression", "logic_term", "logic_factor", 
		"arithmetic", "term", "factor", "lambda", "query", "member_expression"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'function'", "'('", "','", "')'", "'{'", "'}'", "'break'", "';'", 
		"'continue'", "'debugger'", "'while'", "'if'", "'else'", "'return'", "'||'", 
		"'&&'", "'<'", "'<='", "'>'", "'>='", "'=='", "'!='", "'+'", "'-'", "'*'", 
		"'/'", "'null'", "'true'", "'false'", "'['", "']'", "'fun'", "'Select'", 
		"'From'", "'Where'", "'GroupBy'", "'OrderBy'", "'Join'", "'LeftJoin'", 
		"'RightJoin'", "'Limit'", "'='", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, "WS", "COMMENT", "LINE_COMMENT", 
		"IDENTIFIER", "STRING_LITERAL", "NUMERIC_LITERAL"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SimpleLanguage.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	private SLNodeFactory factory;
	private Source source;

	private static final class BailoutErrorListener extends BaseErrorListener {
	    private final Source source;
	    BailoutErrorListener(Source source) {
	        this.source = source;
	    }
	    @Override
	    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
	        String location = "-- line " + line + " col " + (charPositionInLine + 1) + ": ";
	        throw new SLParseError(source, line, charPositionInLine + 1, offendingSymbol == null ? 1 : ((Token) offendingSymbol).getText().length(), String.format("Error(s) parsing script:%n" + location + msg));
	    }
	}

	public void SemErr(Token token, String message) {
	    int col = token.getCharPositionInLine() + 1;
	    String location = "-- line " + token.getLine() + " col " + col + ": ";
	    throw new SLParseError(source, token.getLine(), col, token.getText().length(), String.format("Error(s) parsing script:%n" + location + message));
	}

	public static Map<String, RootCallTarget> parseSL(SLLanguage language, Source source) {
	    SimpleLanguageLexer lexer = new SimpleLanguageLexer(CharStreams.fromString(source.getCharacters().toString()));
	    SimpleLanguageParser parser = new SimpleLanguageParser(new CommonTokenStream(lexer));
	    lexer.removeErrorListeners();
	    parser.removeErrorListeners();
	    BailoutErrorListener listener = new BailoutErrorListener(source);
	    lexer.addErrorListener(listener);
	    parser.addErrorListener(listener);
	    parser.factory = new SLNodeFactory(language, source);
	    parser.source = source;
	    parser.simplelanguage();
	    return parser.factory.getAllFunctions();
	}

	public SimpleLanguageParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SimplelanguageContext extends ParserRuleContext {
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public TerminalNode EOF() { return getToken(SimpleLanguageParser.EOF, 0); }
		public SimplelanguageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simplelanguage; }
	}

	public final SimplelanguageContext simplelanguage() throws RecognitionException {
		SimplelanguageContext _localctx = new SimplelanguageContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_simplelanguage);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(32);
			function();
			setState(36);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(33);
				function();
				}
				}
				setState(38);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(39);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public Token s;
		public BlockContext body;
		public List<TerminalNode> IDENTIFIER() { return getTokens(SimpleLanguageParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SimpleLanguageParser.IDENTIFIER, i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			match(T__0);
			setState(42);
			_localctx.IDENTIFIER = match(IDENTIFIER);
			setState(43);
			_localctx.s = match(T__1);
			 factory.startFunction(_localctx.IDENTIFIER, _localctx.s); 
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(45);
				_localctx.IDENTIFIER = match(IDENTIFIER);
				 factory.addFormalParameter(_localctx.IDENTIFIER); 
				setState(52);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(47);
					match(T__2);
					setState(48);
					_localctx.IDENTIFIER = match(IDENTIFIER);
					 factory.addFormalParameter(_localctx.IDENTIFIER); 
					}
					}
					setState(54);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(57);
			match(T__3);
			setState(58);
			_localctx.body = block(false);
			 factory.finishFunction(_localctx.body.result); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public boolean inLoop;
		public SLStatementNode result;
		public Token s;
		public StatementContext statement;
		public Token e;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BlockContext(ParserRuleContext parent, int invokingState, boolean inLoop) {
			super(parent, invokingState);
			this.inLoop = inLoop;
		}
		@Override public int getRuleIndex() { return RULE_block; }
	}

	public final BlockContext block(boolean inLoop) throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState(), inLoop);
		enterRule(_localctx, 4, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 factory.startBlock();
			                                                  List<SLStatementNode> body = new ArrayList<>(); 
			setState(62);
			_localctx.s = match(T__4);
			setState(68);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__6) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__13) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << IDENTIFIER) | (1L << STRING_LITERAL) | (1L << NUMERIC_LITERAL))) != 0)) {
				{
				{
				setState(63);
				_localctx.statement = statement(inLoop);
				 body.add(_localctx.statement.result); 
				}
				}
				setState(70);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(71);
			_localctx.e = match(T__5);
			 _localctx.result =  factory.finishBlock(body, _localctx.s.getStartIndex(), _localctx.e.getStopIndex() - _localctx.s.getStartIndex() + 1); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public boolean inLoop;
		public SLStatementNode result;
		public While_statementContext while_statement;
		public Token b;
		public Token c;
		public If_statementContext if_statement;
		public Return_statementContext return_statement;
		public ExpressionContext expression;
		public Token d;
		public While_statementContext while_statement() {
			return getRuleContext(While_statementContext.class,0);
		}
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public Return_statementContext return_statement() {
			return getRuleContext(Return_statementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StatementContext(ParserRuleContext parent, int invokingState, boolean inLoop) {
			super(parent, invokingState);
			this.inLoop = inLoop;
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement(boolean inLoop) throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState(), inLoop);
		enterRule(_localctx, 6, RULE_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__10:
				{
				setState(74);
				_localctx.while_statement = while_statement();
				 _localctx.result =  _localctx.while_statement.result; 
				}
				break;
			case T__6:
				{
				setState(77);
				_localctx.b = match(T__6);
				 if (inLoop) { _localctx.result =  factory.createBreak(_localctx.b); } else { SemErr(_localctx.b, "break used outside of loop"); } 
				setState(79);
				match(T__7);
				}
				break;
			case T__8:
				{
				setState(80);
				_localctx.c = match(T__8);
				 if (inLoop) { _localctx.result =  factory.createContinue(_localctx.c); } else { SemErr(_localctx.c, "continue used outside of loop"); } 
				setState(82);
				match(T__7);
				}
				break;
			case T__11:
				{
				setState(83);
				_localctx.if_statement = if_statement(inLoop);
				 _localctx.result =  _localctx.if_statement.result; 
				}
				break;
			case T__13:
				{
				setState(86);
				_localctx.return_statement = return_statement();
				 _localctx.result =  _localctx.return_statement.result; 
				}
				break;
			case T__1:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case IDENTIFIER:
			case STRING_LITERAL:
			case NUMERIC_LITERAL:
				{
				setState(89);
				_localctx.expression = expression();
				setState(90);
				match(T__7);
				 _localctx.result =  _localctx.expression.result; 
				}
				break;
			case T__9:
				{
				setState(93);
				_localctx.d = match(T__9);
				 _localctx.result =  factory.createDebugger(_localctx.d); 
				setState(95);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_statementContext extends ParserRuleContext {
		public SLStatementNode result;
		public Token w;
		public ExpressionContext condition;
		public BlockContext body;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public While_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_statement; }
	}

	public final While_statementContext while_statement() throws RecognitionException {
		While_statementContext _localctx = new While_statementContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_while_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			_localctx.w = match(T__10);
			setState(99);
			match(T__1);
			setState(100);
			_localctx.condition = expression();
			setState(101);
			match(T__3);
			setState(102);
			_localctx.body = block(true);
			 _localctx.result =  factory.createWhile(_localctx.w, _localctx.condition.result, _localctx.body.result); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public boolean inLoop;
		public SLStatementNode result;
		public Token i;
		public ExpressionContext condition;
		public BlockContext then;
		public BlockContext block;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public If_statementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public If_statementContext(ParserRuleContext parent, int invokingState, boolean inLoop) {
			super(parent, invokingState);
			this.inLoop = inLoop;
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
	}

	public final If_statementContext if_statement(boolean inLoop) throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState(), inLoop);
		enterRule(_localctx, 10, RULE_if_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			_localctx.i = match(T__11);
			setState(106);
			match(T__1);
			setState(107);
			_localctx.condition = expression();
			setState(108);
			match(T__3);
			setState(109);
			_localctx.then = _localctx.block = block(inLoop);
			 SLStatementNode elsePart = null; 
			setState(115);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__12) {
				{
				setState(111);
				match(T__12);
				setState(112);
				_localctx.block = block(inLoop);
				 elsePart = _localctx.block.result; 
				}
			}

			 _localctx.result =  factory.createIf(_localctx.i, _localctx.condition.result, _localctx.then.result, elsePart); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_statementContext extends ParserRuleContext {
		public SLStatementNode result;
		public Token r;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Return_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_statement; }
	}

	public final Return_statementContext return_statement() throws RecognitionException {
		Return_statementContext _localctx = new Return_statementContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_return_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(119);
			_localctx.r = match(T__13);
			 SLExpressionNode value = null; 
			setState(124);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << IDENTIFIER) | (1L << STRING_LITERAL) | (1L << NUMERIC_LITERAL))) != 0)) {
				{
				setState(121);
				_localctx.expression = expression();
				 value = _localctx.expression.result; 
				}
			}

			 _localctx.result =  factory.createReturn(_localctx.r, value); 
			setState(127);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public SLExpressionNode result;
		public Logic_termContext logic_term;
		public Token op;
		public List<Logic_termContext> logic_term() {
			return getRuleContexts(Logic_termContext.class);
		}
		public Logic_termContext logic_term(int i) {
			return getRuleContext(Logic_termContext.class,i);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_expression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			_localctx.logic_term = logic_term();
			 _localctx.result =  _localctx.logic_term.result; 
			setState(137);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(131);
					_localctx.op = match(T__14);
					setState(132);
					_localctx.logic_term = logic_term();
					 _localctx.result =  factory.createBinary(_localctx.op, _localctx.result, _localctx.logic_term.result); 
					}
					} 
				}
				setState(139);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logic_termContext extends ParserRuleContext {
		public SLExpressionNode result;
		public Logic_factorContext logic_factor;
		public Token op;
		public List<Logic_factorContext> logic_factor() {
			return getRuleContexts(Logic_factorContext.class);
		}
		public Logic_factorContext logic_factor(int i) {
			return getRuleContext(Logic_factorContext.class,i);
		}
		public Logic_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logic_term; }
	}

	public final Logic_termContext logic_term() throws RecognitionException {
		Logic_termContext _localctx = new Logic_termContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_logic_term);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			_localctx.logic_factor = logic_factor();
			 _localctx.result =  _localctx.logic_factor.result; 
			setState(148);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(142);
					_localctx.op = match(T__15);
					setState(143);
					_localctx.logic_factor = logic_factor();
					 _localctx.result =  factory.createBinary(_localctx.op, _localctx.result, _localctx.logic_factor.result); 
					}
					} 
				}
				setState(150);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logic_factorContext extends ParserRuleContext {
		public SLExpressionNode result;
		public ArithmeticContext arithmetic;
		public Token op;
		public List<ArithmeticContext> arithmetic() {
			return getRuleContexts(ArithmeticContext.class);
		}
		public ArithmeticContext arithmetic(int i) {
			return getRuleContext(ArithmeticContext.class,i);
		}
		public Logic_factorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logic_factor; }
	}

	public final Logic_factorContext logic_factor() throws RecognitionException {
		Logic_factorContext _localctx = new Logic_factorContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_logic_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(151);
			_localctx.arithmetic = arithmetic();
			 _localctx.result =  _localctx.arithmetic.result; 
			setState(157);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(153);
				_localctx.op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21))) != 0)) ) {
					_localctx.op = _errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(154);
				_localctx.arithmetic = arithmetic();
				 _localctx.result =  factory.createBinary(_localctx.op, _localctx.result, _localctx.arithmetic.result); 
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticContext extends ParserRuleContext {
		public SLExpressionNode result;
		public TermContext term;
		public Token op;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public ArithmeticContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic; }
	}

	public final ArithmeticContext arithmetic() throws RecognitionException {
		ArithmeticContext _localctx = new ArithmeticContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_arithmetic);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			_localctx.term = term();
			 _localctx.result =  _localctx.term.result; 
			setState(167);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(161);
					_localctx.op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__22 || _la==T__23) ) {
						_localctx.op = _errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(162);
					_localctx.term = term();
					 _localctx.result =  factory.createBinary(_localctx.op, _localctx.result, _localctx.term.result); 
					}
					} 
				}
				setState(169);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public SLExpressionNode result;
		public FactorContext factor;
		public Token op;
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_term);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			_localctx.factor = factor();
			 _localctx.result =  _localctx.factor.result; 
			setState(178);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(172);
					_localctx.op = _input.LT(1);
					_la = _input.LA(1);
					if ( !(_la==T__24 || _la==T__25) ) {
						_localctx.op = _errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(173);
					_localctx.factor = factor();
					 _localctx.result =  factory.createBinary(_localctx.op, _localctx.result, _localctx.factor.result); 
					}
					} 
				}
				setState(180);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public SLExpressionNode result;
		public Token t;
		public Token IDENTIFIER;
		public Member_expressionContext member_expression;
		public LambdaContext lambda;
		public QueryContext query;
		public ExpressionContext expression;
		public Token STRING_LITERAL;
		public Token NUMERIC_LITERAL;
		public Token s;
		public ExpressionContext expr;
		public Token e;
		public TerminalNode IDENTIFIER() { return getToken(SimpleLanguageParser.IDENTIFIER, 0); }
		public LambdaContext lambda() {
			return getRuleContext(LambdaContext.class,0);
		}
		public QueryContext query() {
			return getRuleContext(QueryContext.class,0);
		}
		public TerminalNode STRING_LITERAL() { return getToken(SimpleLanguageParser.STRING_LITERAL, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(SimpleLanguageParser.NUMERIC_LITERAL, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Member_expressionContext member_expression() {
			return getRuleContext(Member_expressionContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__26:
				{
				setState(181);
				_localctx.t = match(T__26);
				 _localctx.result =  factory.createNull(_localctx.t); 
				}
				break;
			case T__27:
			case T__28:
				{
				setState(183);
				_localctx.t = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==T__27 || _la==T__28) ) {
					_localctx.t = _errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				 _localctx.result =  factory.createBoolean(_localctx.t); 
				}
				break;
			case IDENTIFIER:
				{
				setState(185);
				_localctx.IDENTIFIER = match(IDENTIFIER);
				 SLExpressionNode assignmentName = factory.createStringLiteral(_localctx.IDENTIFIER, false); 
				setState(191);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
				case 1:
					{
					setState(187);
					_localctx.member_expression = member_expression(null, null, assignmentName);
					 _localctx.result =  _localctx.member_expression.result; 
					}
					break;
				case 2:
					{
					 _localctx.result =  factory.createRead(assignmentName); 
					}
					break;
				}
				}
				break;
			case T__31:
				{
				setState(193);
				_localctx.lambda = lambda();
				 SLExpressionNode assignmentName = factory.createStringLiteral(_localctx.lambda.result, false); 
				setState(199);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
				case 1:
					{
					setState(195);
					_localctx.member_expression = member_expression(null, null, assignmentName);
					 _localctx.result =  _localctx.member_expression.result; 
					}
					break;
				case 2:
					{
					 _localctx.result =  factory.createRead(assignmentName); 
					}
					break;
				}
				}
				break;
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
				{
				setState(201);
				_localctx.query = query();
				 _localctx.result =  _localctx.query.result; 
				}
				break;
			case T__29:
				{
				setState(204);
				_localctx.t = match(T__29);
				 List<SLExpressionNode> elements = new ArrayList<>(); 
				setState(217);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << IDENTIFIER) | (1L << STRING_LITERAL) | (1L << NUMERIC_LITERAL))) != 0)) {
					{
					setState(206);
					_localctx.expression = expression();
					 elements.add(_localctx.expression.result); 
					setState(214);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__2) {
						{
						{
						setState(208);
						match(T__2);
						setState(209);
						_localctx.expression = expression();
						 elements.add(_localctx.expression.result); 
						}
						}
						setState(216);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(219);
				match(T__30);
				 _localctx.result =  factory.createList(_localctx.t, elements); 
				}
				break;
			case STRING_LITERAL:
				{
				setState(221);
				_localctx.STRING_LITERAL = match(STRING_LITERAL);
				 _localctx.result =  factory.createStringLiteral(_localctx.STRING_LITERAL, true); 
				}
				break;
			case NUMERIC_LITERAL:
				{
				setState(223);
				_localctx.NUMERIC_LITERAL = match(NUMERIC_LITERAL);
				 _localctx.result =  factory.createNumericLiteral(_localctx.NUMERIC_LITERAL); 
				}
				break;
			case T__1:
				{
				setState(225);
				_localctx.s = match(T__1);
				setState(226);
				_localctx.expr = _localctx.expression = expression();
				setState(227);
				_localctx.e = match(T__3);
				 _localctx.result =  factory.createParenExpression(_localctx.expr.result, _localctx.s.getStartIndex(), _localctx.e.getStopIndex() - _localctx.s.getStartIndex() + 1); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LambdaContext extends ParserRuleContext {
		public Token result;
		public Token identifierToken;
		public Token t;
		public Token IDENTIFIER;
		public BlockContext block;
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(SimpleLanguageParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SimpleLanguageParser.IDENTIFIER, i);
		}
		public LambdaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda; }
	}

	public final LambdaContext lambda() throws RecognitionException {
		LambdaContext _localctx = new LambdaContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_lambda);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(232);
			_localctx.identifierToken = match(T__31);
			setState(233);
			_localctx.t = match(T__1);
			 final String name = factory.startLambda(_localctx.identifierToken, _localctx.t); 
			setState(245);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(235);
				_localctx.IDENTIFIER = match(IDENTIFIER);
				 factory.addFormalParameter(_localctx.IDENTIFIER); 
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(237);
					match(T__2);
					setState(238);
					_localctx.IDENTIFIER = match(IDENTIFIER);
					 factory.addFormalParameter(_localctx.IDENTIFIER); 
					}
					}
					setState(244);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(247);
			match(T__3);
			setState(248);
			_localctx.block = block(false);
			 factory.finishFunction(_localctx.block.result);
															  _localctx.result =  new CommonToken(_localctx.identifierToken.getType(), name); 
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public SLExpressionNode result;
		public Token begin;
		public ExpressionContext formatter;
		public ExpressionContext q;
		public ExpressionContext table;
		public ExpressionContext filter;
		public ExpressionContext group;
		public ExpressionContext order;
		public ExpressionContext isAscending;
		public ExpressionContext left;
		public ExpressionContext right;
		public ExpressionContext limit;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(329);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__32:
				{
				setState(251);
				_localctx.begin = match(T__32);
				setState(252);
				match(T__1);
				setState(253);
				_localctx.formatter = expression();
				setState(254);
				match(T__2);
				setState(255);
				_localctx.q = expression();
				setState(256);
				match(T__3);
				 _localctx.result =  factory.createSelect(_localctx.begin, _localctx.formatter.result, _localctx.q.result); 
				}
				break;
			case T__33:
				{
				setState(259);
				_localctx.begin = match(T__33);
				setState(260);
				match(T__1);
				setState(261);
				_localctx.table = expression();
				setState(262);
				match(T__3);
				 _localctx.result =  factory.createFrom(_localctx.begin, _localctx.table.result); 
				}
				break;
			case T__34:
				{
				setState(265);
				_localctx.begin = match(T__34);
				setState(266);
				match(T__1);
				setState(267);
				_localctx.filter = expression();
				setState(268);
				match(T__2);
				setState(269);
				_localctx.q = expression();
				setState(270);
				match(T__3);
				 _localctx.result =  factory.createWhere(_localctx.begin, _localctx.filter.result, _localctx.q.result); 
				}
				break;
			case T__35:
				{
				setState(273);
				_localctx.begin = match(T__35);
				setState(274);
				match(T__1);
				setState(275);
				_localctx.group = expression();
				setState(276);
				match(T__2);
				setState(277);
				_localctx.q = expression();
				setState(278);
				match(T__3);
				 _localctx.result =  factory.createGroup(_localctx.begin, _localctx.group.result, _localctx.q.result); 
				}
				break;
			case T__36:
				{
				setState(281);
				_localctx.begin = match(T__36);
				setState(282);
				match(T__1);
				setState(283);
				_localctx.order = expression();
				setState(284);
				match(T__2);
				setState(285);
				_localctx.isAscending = expression();
				setState(286);
				match(T__2);
				setState(287);
				_localctx.q = expression();
				setState(288);
				match(T__3);
				 _localctx.result =  factory.createOrder(_localctx.begin, _localctx.order.result, _localctx.isAscending.result, _localctx.q.result); 
				}
				break;
			case T__37:
				{
				setState(291);
				_localctx.begin = match(T__37);
				setState(292);
				match(T__1);
				setState(293);
				_localctx.left = expression();
				setState(294);
				match(T__2);
				setState(295);
				_localctx.right = expression();
				setState(296);
				match(T__2);
				setState(297);
				_localctx.filter = expression();
				setState(298);
				match(T__3);
				 _localctx.result =  factory.createJoin(_localctx.begin, _localctx.left.result, _localctx.right.result, _localctx.filter.result); 
				}
				break;
			case T__38:
				{
				setState(301);
				_localctx.begin = match(T__38);
				setState(302);
				match(T__1);
				setState(303);
				_localctx.left = expression();
				setState(304);
				match(T__2);
				setState(305);
				_localctx.right = expression();
				setState(306);
				match(T__2);
				setState(307);
				_localctx.filter = expression();
				setState(308);
				match(T__3);
				 _localctx.result =  factory.createLeftJoin(_localctx.begin, _localctx.left.result, _localctx.right.result, _localctx.filter.result); 
				}
				break;
			case T__39:
				{
				setState(311);
				_localctx.begin = match(T__39);
				setState(312);
				match(T__1);
				setState(313);
				_localctx.left = expression();
				setState(314);
				match(T__2);
				setState(315);
				_localctx.right = expression();
				setState(316);
				match(T__2);
				setState(317);
				_localctx.filter = expression();
				setState(318);
				match(T__3);
				 _localctx.result =  factory.createRightJoin(_localctx.begin, _localctx.left.result, _localctx.right.result, _localctx.filter.result); 
				}
				break;
			case T__40:
				{
				setState(321);
				_localctx.begin = match(T__40);
				setState(322);
				match(T__1);
				setState(323);
				_localctx.limit = expression();
				setState(324);
				match(T__2);
				setState(325);
				_localctx.q = expression();
				setState(326);
				match(T__3);
				 _localctx.result =  factory.createLimit(_localctx.begin, _localctx.limit.result, _localctx.q.result); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Member_expressionContext extends ParserRuleContext {
		public SLExpressionNode r;
		public SLExpressionNode assignmentReceiver;
		public SLExpressionNode assignmentName;
		public SLExpressionNode result;
		public ExpressionContext expression;
		public Token e;
		public Token IDENTIFIER;
		public Member_expressionContext member_expression;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode IDENTIFIER() { return getToken(SimpleLanguageParser.IDENTIFIER, 0); }
		public Member_expressionContext member_expression() {
			return getRuleContext(Member_expressionContext.class,0);
		}
		public Member_expressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public Member_expressionContext(ParserRuleContext parent, int invokingState, SLExpressionNode r, SLExpressionNode assignmentReceiver, SLExpressionNode assignmentName) {
			super(parent, invokingState);
			this.r = r;
			this.assignmentReceiver = assignmentReceiver;
			this.assignmentName = assignmentName;
		}
		@Override public int getRuleIndex() { return RULE_member_expression; }
	}

	public final Member_expressionContext member_expression(SLExpressionNode r,SLExpressionNode assignmentReceiver,SLExpressionNode assignmentName) throws RecognitionException {
		Member_expressionContext _localctx = new Member_expressionContext(_ctx, getState(), r, assignmentReceiver, assignmentName);
		enterRule(_localctx, 30, RULE_member_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 SLExpressionNode receiver = r;
			                                                  SLExpressionNode nestedAssignmentName = null; 
			setState(363);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(332);
				match(T__1);
				 List<SLExpressionNode> parameters = new ArrayList<>();
				                                                  if (receiver == null) {
				                                                      receiver = factory.createRead(assignmentName);
				                                                  } 
				setState(345);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << IDENTIFIER) | (1L << STRING_LITERAL) | (1L << NUMERIC_LITERAL))) != 0)) {
					{
					setState(334);
					_localctx.expression = expression();
					 parameters.add(_localctx.expression.result); 
					setState(342);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__2) {
						{
						{
						setState(336);
						match(T__2);
						setState(337);
						_localctx.expression = expression();
						 parameters.add(_localctx.expression.result); 
						}
						}
						setState(344);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(347);
				_localctx.e = match(T__3);
				 _localctx.result =  factory.createCall(receiver, parameters, _localctx.e); 
				}
				break;
			case T__41:
				{
				setState(349);
				match(T__41);
				setState(350);
				_localctx.expression = expression();
				 if (assignmentName == null) {
				                                                      SemErr((_localctx.expression!=null?(_localctx.expression.start):null), "invalid assignment target");
				                                                  } else if (assignmentReceiver == null) {
				                                                      _localctx.result =  factory.createAssignment(assignmentName, _localctx.expression.result);
				                                                  } else {
				                                                      _localctx.result =  factory.createWriteProperty(assignmentReceiver, assignmentName, _localctx.expression.result);
				                                                  } 
				}
				break;
			case T__42:
				{
				setState(353);
				match(T__42);
				 if (receiver == null) {
				                                                       receiver = factory.createRead(assignmentName);
				                                                  } 
				setState(355);
				_localctx.IDENTIFIER = match(IDENTIFIER);
				 nestedAssignmentName = factory.createStringLiteral(_localctx.IDENTIFIER, false);
				                                                  _localctx.result =  factory.createReadProperty(receiver, nestedAssignmentName); 
				}
				break;
			case T__29:
				{
				setState(357);
				match(T__29);
				 if (receiver == null) {
				                                                      receiver = factory.createRead(assignmentName);
				                                                  } 
				setState(359);
				_localctx.expression = expression();
				 nestedAssignmentName = _localctx.expression.result;
				                                                  _localctx.result =  factory.createReadProperty(receiver, nestedAssignmentName); 
				setState(361);
				match(T__30);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(368);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
				setState(365);
				_localctx.member_expression = member_expression(_localctx.result, receiver, nestedAssignmentName);
				 _localctx.result =  _localctx.member_expression.result; 
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\63\u0175\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2"+
		"\7\2%\n\2\f\2\16\2(\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7"+
		"\3\65\n\3\f\3\16\38\13\3\5\3:\n\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4"+
		"\7\4E\n\4\f\4\16\4H\13\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5c\n\5\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7v\n"+
		"\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\5\b\177\n\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\7\t\u008a\n\t\f\t\16\t\u008d\13\t\3\n\3\n\3\n\3\n\3\n\3\n\7\n"+
		"\u0095\n\n\f\n\16\n\u0098\13\n\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00a0"+
		"\n\13\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00a8\n\f\f\f\16\f\u00ab\13\f\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\7\r\u00b3\n\r\f\r\16\r\u00b6\13\r\3\16\3\16\3\16\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00c2\n\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\5\16\u00ca\n\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\7\16\u00d7\n\16\f\16\16\16\u00da\13\16\5\16\u00dc\n\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00e9\n\16\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00f3\n\17\f\17\16\17\u00f6\13"+
		"\17\5\17\u00f8\n\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20"+
		"\3\20\3\20\5\20\u014c\n\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\7\21\u0157\n\21\f\21\16\21\u015a\13\21\5\21\u015c\n\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21"+
		"\u016e\n\21\3\21\3\21\3\21\5\21\u0173\n\21\3\21\2\2\22\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \2\6\3\2\23\30\3\2\31\32\3\2\33\34\3\2\36\37"+
		"\2\u0191\2\"\3\2\2\2\4+\3\2\2\2\6?\3\2\2\2\bb\3\2\2\2\nd\3\2\2\2\fk\3"+
		"\2\2\2\16y\3\2\2\2\20\u0083\3\2\2\2\22\u008e\3\2\2\2\24\u0099\3\2\2\2"+
		"\26\u00a1\3\2\2\2\30\u00ac\3\2\2\2\32\u00e8\3\2\2\2\34\u00ea\3\2\2\2\36"+
		"\u014b\3\2\2\2 \u014d\3\2\2\2\"&\5\4\3\2#%\5\4\3\2$#\3\2\2\2%(\3\2\2\2"+
		"&$\3\2\2\2&\'\3\2\2\2\')\3\2\2\2(&\3\2\2\2)*\7\2\2\3*\3\3\2\2\2+,\7\3"+
		"\2\2,-\7\61\2\2-.\7\4\2\2.9\b\3\1\2/\60\7\61\2\2\60\66\b\3\1\2\61\62\7"+
		"\5\2\2\62\63\7\61\2\2\63\65\b\3\1\2\64\61\3\2\2\2\658\3\2\2\2\66\64\3"+
		"\2\2\2\66\67\3\2\2\2\67:\3\2\2\28\66\3\2\2\29/\3\2\2\29:\3\2\2\2:;\3\2"+
		"\2\2;<\7\6\2\2<=\5\6\4\2=>\b\3\1\2>\5\3\2\2\2?@\b\4\1\2@F\7\7\2\2AB\5"+
		"\b\5\2BC\b\4\1\2CE\3\2\2\2DA\3\2\2\2EH\3\2\2\2FD\3\2\2\2FG\3\2\2\2GI\3"+
		"\2\2\2HF\3\2\2\2IJ\7\b\2\2JK\b\4\1\2K\7\3\2\2\2LM\5\n\6\2MN\b\5\1\2Nc"+
		"\3\2\2\2OP\7\t\2\2PQ\b\5\1\2Qc\7\n\2\2RS\7\13\2\2ST\b\5\1\2Tc\7\n\2\2"+
		"UV\5\f\7\2VW\b\5\1\2Wc\3\2\2\2XY\5\16\b\2YZ\b\5\1\2Zc\3\2\2\2[\\\5\20"+
		"\t\2\\]\7\n\2\2]^\b\5\1\2^c\3\2\2\2_`\7\f\2\2`a\b\5\1\2ac\7\n\2\2bL\3"+
		"\2\2\2bO\3\2\2\2bR\3\2\2\2bU\3\2\2\2bX\3\2\2\2b[\3\2\2\2b_\3\2\2\2c\t"+
		"\3\2\2\2de\7\r\2\2ef\7\4\2\2fg\5\20\t\2gh\7\6\2\2hi\5\6\4\2ij\b\6\1\2"+
		"j\13\3\2\2\2kl\7\16\2\2lm\7\4\2\2mn\5\20\t\2no\7\6\2\2op\5\6\4\2pu\b\7"+
		"\1\2qr\7\17\2\2rs\5\6\4\2st\b\7\1\2tv\3\2\2\2uq\3\2\2\2uv\3\2\2\2vw\3"+
		"\2\2\2wx\b\7\1\2x\r\3\2\2\2yz\7\20\2\2z~\b\b\1\2{|\5\20\t\2|}\b\b\1\2"+
		"}\177\3\2\2\2~{\3\2\2\2~\177\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\b\b"+
		"\1\2\u0081\u0082\7\n\2\2\u0082\17\3\2\2\2\u0083\u0084\5\22\n\2\u0084\u008b"+
		"\b\t\1\2\u0085\u0086\7\21\2\2\u0086\u0087\5\22\n\2\u0087\u0088\b\t\1\2"+
		"\u0088\u008a\3\2\2\2\u0089\u0085\3\2\2\2\u008a\u008d\3\2\2\2\u008b\u0089"+
		"\3\2\2\2\u008b\u008c\3\2\2\2\u008c\21\3\2\2\2\u008d\u008b\3\2\2\2\u008e"+
		"\u008f\5\24\13\2\u008f\u0096\b\n\1\2\u0090\u0091\7\22\2\2\u0091\u0092"+
		"\5\24\13\2\u0092\u0093\b\n\1\2\u0093\u0095\3\2\2\2\u0094\u0090\3\2\2\2"+
		"\u0095\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\23"+
		"\3\2\2\2\u0098\u0096\3\2\2\2\u0099\u009a\5\26\f\2\u009a\u009f\b\13\1\2"+
		"\u009b\u009c\t\2\2\2\u009c\u009d\5\26\f\2\u009d\u009e\b\13\1\2\u009e\u00a0"+
		"\3\2\2\2\u009f\u009b\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\25\3\2\2\2\u00a1"+
		"\u00a2\5\30\r\2\u00a2\u00a9\b\f\1\2\u00a3\u00a4\t\3\2\2\u00a4\u00a5\5"+
		"\30\r\2\u00a5\u00a6\b\f\1\2\u00a6\u00a8\3\2\2\2\u00a7\u00a3\3\2\2\2\u00a8"+
		"\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\27\3\2\2"+
		"\2\u00ab\u00a9\3\2\2\2\u00ac\u00ad\5\32\16\2\u00ad\u00b4\b\r\1\2\u00ae"+
		"\u00af\t\4\2\2\u00af\u00b0\5\32\16\2\u00b0\u00b1\b\r\1\2\u00b1\u00b3\3"+
		"\2\2\2\u00b2\u00ae\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b4"+
		"\u00b5\3\2\2\2\u00b5\31\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b7\u00b8\7\35\2"+
		"\2\u00b8\u00e9\b\16\1\2\u00b9\u00ba\t\5\2\2\u00ba\u00e9\b\16\1\2\u00bb"+
		"\u00bc\7\61\2\2\u00bc\u00c1\b\16\1\2\u00bd\u00be\5 \21\2\u00be\u00bf\b"+
		"\16\1\2\u00bf\u00c2\3\2\2\2\u00c0\u00c2\b\16\1\2\u00c1\u00bd\3\2\2\2\u00c1"+
		"\u00c0\3\2\2\2\u00c2\u00e9\3\2\2\2\u00c3\u00c4\5\34\17\2\u00c4\u00c9\b"+
		"\16\1\2\u00c5\u00c6\5 \21\2\u00c6\u00c7\b\16\1\2\u00c7\u00ca\3\2\2\2\u00c8"+
		"\u00ca\b\16\1\2\u00c9\u00c5\3\2\2\2\u00c9\u00c8\3\2\2\2\u00ca\u00e9\3"+
		"\2\2\2\u00cb\u00cc\5\36\20\2\u00cc\u00cd\b\16\1\2\u00cd\u00e9\3\2\2\2"+
		"\u00ce\u00cf\7 \2\2\u00cf\u00db\b\16\1\2\u00d0\u00d1\5\20\t\2\u00d1\u00d8"+
		"\b\16\1\2\u00d2\u00d3\7\5\2\2\u00d3\u00d4\5\20\t\2\u00d4\u00d5\b\16\1"+
		"\2\u00d5\u00d7\3\2\2\2\u00d6\u00d2\3\2\2\2\u00d7\u00da\3\2\2\2\u00d8\u00d6"+
		"\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3\2\2\2\u00db"+
		"\u00d0\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de\7!"+
		"\2\2\u00de\u00e9\b\16\1\2\u00df\u00e0\7\62\2\2\u00e0\u00e9\b\16\1\2\u00e1"+
		"\u00e2\7\63\2\2\u00e2\u00e9\b\16\1\2\u00e3\u00e4\7\4\2\2\u00e4\u00e5\5"+
		"\20\t\2\u00e5\u00e6\7\6\2\2\u00e6\u00e7\b\16\1\2\u00e7\u00e9\3\2\2\2\u00e8"+
		"\u00b7\3\2\2\2\u00e8\u00b9\3\2\2\2\u00e8\u00bb\3\2\2\2\u00e8\u00c3\3\2"+
		"\2\2\u00e8\u00cb\3\2\2\2\u00e8\u00ce\3\2\2\2\u00e8\u00df\3\2\2\2\u00e8"+
		"\u00e1\3\2\2\2\u00e8\u00e3\3\2\2\2\u00e9\33\3\2\2\2\u00ea\u00eb\7\"\2"+
		"\2\u00eb\u00ec\7\4\2\2\u00ec\u00f7\b\17\1\2\u00ed\u00ee\7\61\2\2\u00ee"+
		"\u00f4\b\17\1\2\u00ef\u00f0\7\5\2\2\u00f0\u00f1\7\61\2\2\u00f1\u00f3\b"+
		"\17\1\2\u00f2\u00ef\3\2\2\2\u00f3\u00f6\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4"+
		"\u00f5\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f7\u00ed\3\2"+
		"\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fa\7\6\2\2\u00fa"+
		"\u00fb\5\6\4\2\u00fb\u00fc\b\17\1\2\u00fc\35\3\2\2\2\u00fd\u00fe\7#\2"+
		"\2\u00fe\u00ff\7\4\2\2\u00ff\u0100\5\20\t\2\u0100\u0101\7\5\2\2\u0101"+
		"\u0102\5\20\t\2\u0102\u0103\7\6\2\2\u0103\u0104\b\20\1\2\u0104\u014c\3"+
		"\2\2\2\u0105\u0106\7$\2\2\u0106\u0107\7\4\2\2\u0107\u0108\5\20\t\2\u0108"+
		"\u0109\7\6\2\2\u0109\u010a\b\20\1\2\u010a\u014c\3\2\2\2\u010b\u010c\7"+
		"%\2\2\u010c\u010d\7\4\2\2\u010d\u010e\5\20\t\2\u010e\u010f\7\5\2\2\u010f"+
		"\u0110\5\20\t\2\u0110\u0111\7\6\2\2\u0111\u0112\b\20\1\2\u0112\u014c\3"+
		"\2\2\2\u0113\u0114\7&\2\2\u0114\u0115\7\4\2\2\u0115\u0116\5\20\t\2\u0116"+
		"\u0117\7\5\2\2\u0117\u0118\5\20\t\2\u0118\u0119\7\6\2\2\u0119\u011a\b"+
		"\20\1\2\u011a\u014c\3\2\2\2\u011b\u011c\7\'\2\2\u011c\u011d\7\4\2\2\u011d"+
		"\u011e\5\20\t\2\u011e\u011f\7\5\2\2\u011f\u0120\5\20\t\2\u0120\u0121\7"+
		"\5\2\2\u0121\u0122\5\20\t\2\u0122\u0123\7\6\2\2\u0123\u0124\b\20\1\2\u0124"+
		"\u014c\3\2\2\2\u0125\u0126\7(\2\2\u0126\u0127\7\4\2\2\u0127\u0128\5\20"+
		"\t\2\u0128\u0129\7\5\2\2\u0129\u012a\5\20\t\2\u012a\u012b\7\5\2\2\u012b"+
		"\u012c\5\20\t\2\u012c\u012d\7\6\2\2\u012d\u012e\b\20\1\2\u012e\u014c\3"+
		"\2\2\2\u012f\u0130\7)\2\2\u0130\u0131\7\4\2\2\u0131\u0132\5\20\t\2\u0132"+
		"\u0133\7\5\2\2\u0133\u0134\5\20\t\2\u0134\u0135\7\5\2\2\u0135\u0136\5"+
		"\20\t\2\u0136\u0137\7\6\2\2\u0137\u0138\b\20\1\2\u0138\u014c\3\2\2\2\u0139"+
		"\u013a\7*\2\2\u013a\u013b\7\4\2\2\u013b\u013c\5\20\t\2\u013c\u013d\7\5"+
		"\2\2\u013d\u013e\5\20\t\2\u013e\u013f\7\5\2\2\u013f\u0140\5\20\t\2\u0140"+
		"\u0141\7\6\2\2\u0141\u0142\b\20\1\2\u0142\u014c\3\2\2\2\u0143\u0144\7"+
		"+\2\2\u0144\u0145\7\4\2\2\u0145\u0146\5\20\t\2\u0146\u0147\7\5\2\2\u0147"+
		"\u0148\5\20\t\2\u0148\u0149\7\6\2\2\u0149\u014a\b\20\1\2\u014a\u014c\3"+
		"\2\2\2\u014b\u00fd\3\2\2\2\u014b\u0105\3\2\2\2\u014b\u010b\3\2\2\2\u014b"+
		"\u0113\3\2\2\2\u014b\u011b\3\2\2\2\u014b\u0125\3\2\2\2\u014b\u012f\3\2"+
		"\2\2\u014b\u0139\3\2\2\2\u014b\u0143\3\2\2\2\u014c\37\3\2\2\2\u014d\u016d"+
		"\b\21\1\2\u014e\u014f\7\4\2\2\u014f\u015b\b\21\1\2\u0150\u0151\5\20\t"+
		"\2\u0151\u0158\b\21\1\2\u0152\u0153\7\5\2\2\u0153\u0154\5\20\t\2\u0154"+
		"\u0155\b\21\1\2\u0155\u0157\3\2\2\2\u0156\u0152\3\2\2\2\u0157\u015a\3"+
		"\2\2\2\u0158\u0156\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015c\3\2\2\2\u015a"+
		"\u0158\3\2\2\2\u015b\u0150\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\3\2"+
		"\2\2\u015d\u015e\7\6\2\2\u015e\u016e\b\21\1\2\u015f\u0160\7,\2\2\u0160"+
		"\u0161\5\20\t\2\u0161\u0162\b\21\1\2\u0162\u016e\3\2\2\2\u0163\u0164\7"+
		"-\2\2\u0164\u0165\b\21\1\2\u0165\u0166\7\61\2\2\u0166\u016e\b\21\1\2\u0167"+
		"\u0168\7 \2\2\u0168\u0169\b\21\1\2\u0169\u016a\5\20\t\2\u016a\u016b\b"+
		"\21\1\2\u016b\u016c\7!\2\2\u016c\u016e\3\2\2\2\u016d\u014e\3\2\2\2\u016d"+
		"\u015f\3\2\2\2\u016d\u0163\3\2\2\2\u016d\u0167\3\2\2\2\u016e\u0172\3\2"+
		"\2\2\u016f\u0170\5 \21\2\u0170\u0171\b\21\1\2\u0171\u0173\3\2\2\2\u0172"+
		"\u016f\3\2\2\2\u0172\u0173\3\2\2\2\u0173!\3\2\2\2\32&\669Fbu~\u008b\u0096"+
		"\u009f\u00a9\u00b4\u00c1\u00c9\u00d8\u00db\u00e8\u00f4\u00f7\u014b\u0158"+
		"\u015b\u016d\u0172";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
