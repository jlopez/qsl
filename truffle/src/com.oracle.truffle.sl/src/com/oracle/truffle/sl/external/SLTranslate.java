package com.oracle.truffle.sl.external;

import java.io.IOException;
import java.io.Serializable;

import org.apache.hadoop.hive.ql.exec.UDF;

import com.oracle.truffle.sl.runtime.SLValue;

public class SLTranslate extends UDF {
    public static SLValue evaluate(final Serializable v) throws IOException {
        return new SLValue(v);
    }
}
