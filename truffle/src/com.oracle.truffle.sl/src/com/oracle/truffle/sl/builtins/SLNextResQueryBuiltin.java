/*
 * Copyright (c) 2012, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.sl.builtins;

import qir.ast.QIRNode;

import java.util.List;
import java.util.Map;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.*;
import com.oracle.truffle.api.nodes.*;
import com.oracle.truffle.api.object.*;
import com.oracle.truffle.sl.SLException;
import com.oracle.truffle.sl.nodes.SLNodeVisitor;
import com.oracle.truffle.sl.qirinterface.QIRInterface;
import com.oracle.truffle.sl.runtime.SLContext;
import com.oracle.truffle.sl.runtime.SLNull;

/**
 * This builtin takes an object returned by a query and returns a tuple from the result. This tuple
 * is an object with a "get" method to fetch a column given the name.
 */
@NodeInfo(shortName = "next")
public abstract class SLNextResQueryBuiltin extends SLBuiltinNode {
    public SLNextResQueryBuiltin() {
    }

    @Specialization
    public final Object next(final DynamicObject o) {
        return doNext(o);
    }

    @TruffleBoundary
    private final Object doNext(final DynamicObject o) {
        final SLContext context = getContext();
        // Every query is identified by an integer included in the object returned by the query.
        final int queryId = ((Long) o.get("queryId", SLContext.INVALID_QUERY_ID)).intValue();
        if (queryId == SLContext.INVALID_QUERY_ID)
            throw new SLException("Not a query object.", this);

        // TODO: Handle results given in streaming.
        QIRNode query = context.queries.get(queryId);
        if (query == null)
            throw new SLException("Unrecognized or empty query: " + queryId, this);

        List<Map<String, Object>> queryResults = context.results.get(queryId);
        if (queryResults == null) { // The query hasn't been run yet.
            query = QIRInterface.normalize(query, context.envs.get(queryId), context);
            context.queries.set(queryId, query);
            queryResults = QIRInterface.run(query);
            context.results.set(queryId, queryResults);
        }
        if (queryResults.isEmpty())
            return SLNull.SINGLETON;

        final DynamicObject res = context.createObject();
        queryResults.remove(0).entrySet().stream().forEach(pair -> res.define(pair.getKey(), pair.getValue()));
        return res;
    }

    @Override
    public final <T> T accept(SLNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}