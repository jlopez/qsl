package com.oracle.truffle.sl.external;

import java.io.IOException;
import java.util.List;
import org.apache.hadoop.hive.ql.exec.UDF;

import com.oracle.truffle.sl.SLLanguage;

public class SLExecuteApply extends UDF {
    public static String evaluate(final String fun, final List<String> args) {
        try {
            return SLLanguage.executeApply(fun, args.toArray(new String[]{}));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
