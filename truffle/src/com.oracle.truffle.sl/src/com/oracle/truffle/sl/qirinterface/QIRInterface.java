/*
 * Copyright (c) 2015, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.sl.qirinterface;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

import java.util.function.BiFunction;

import com.oracle.truffle.api.frame.Frame;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.sl.SLException;
import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.nodes.SLRootNode;
import com.oracle.truffle.sl.nodes.SLUndefinedFunctionRootNode;
import com.oracle.truffle.sl.runtime.SLContext;
import com.oracle.truffle.sl.runtime.SLFunction;
import com.oracle.truffle.sl.runtime.SLNull;
import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.driver.QIRDriver;

public final class QIRInterface {
    /**
     * Runs a query in the database pointed by the given driver and returns the results.
     *
     * @param query The query to be executed in the targeted database
     * @return The results of the query
     */
    public static final List<Map<String, Object>> run(final QIRNode query) {
        QIRNode qirRes = QIRDriver.run(query);
        if (qirRes == null)
            throw new SLException("Could not run query: " + query, null);
        final List<Map<String, Object>> result = new ArrayList<>();
        while (qirRes != QIRLnil.getInstance()) {
            final Map<String, Object> tuple = new HashMap<>();
            QIRNode qirTuple = ((QIRLcons) qirRes).getValue();
            while (qirTuple != QIRRnil.getInstance()) {
                tuple.put(((QIRRcons) qirTuple).getId(), QIRToSLType(((QIRRcons) qirTuple).getValue()));
                qirTuple = ((QIRRcons) qirTuple).getTail();
            }
            result.add(tuple);
            qirRes = ((QIRLcons) qirRes).getTail();
        }
        return result;
    }

    /**
     * Resolves the free variables of a query.
     *
     * @param arg The query to be executed in the targeted database
     * @param frame The environment of execution
     * @param context The other part of the environment of execution
     * @return The closed query
     */
    public static final QIRNode normalize(final QIRNode arg, final Frame frame, final SLContext context) {
        QIRNode query = arg;
        for (Map<String, QIRVariable> fvs = QIRDriver.getFreeVars(query); !fvs.isEmpty(); fvs = QIRDriver.getFreeVars(query))
            for (final QIRVariable fv : fvs.values()) {
                final String varName = fv.id;
                final FrameSlot varSlot = fv.slot;
                query = new QIRApply(null, new QIRLambda(null, null, new QIRVariable(null, varName, varSlot), query, new FrameDescriptor()),
                                SLToQIRType.apply(context).apply(fv.getSourceSection(), varSlot != null ? frame.getValue(varSlot) : context.getFunctionRegistry().lookup(varName, true)));
            }
        return query;
    }

    /**
     * Translates a QIR expression into a SL statement.
     *
     * @param value The QIR expression to translate
     * @return The translation of the QIR expression in SL
     * @throws SLException If the type of the value is not supported.
     */
    static final Object QIRToSLType(final QIRNode value) throws SLException {
        if (value instanceof QIRBaseValue<?>)
            return ((QIRBaseValue<?>) value).getValue();
        throw new SLException("Unsupported value: " + value, null);
    }

    /**
     * Translates a SL statement into a QIR expression.
     *
     * @param context The environment in which the SL statement was made
     * @param src The {@link SourceSection} of the given value
     * @param value The SL statement to translate
     * @return The translation of the SL statement in QIR
     */
    static final Function<SLContext, BiFunction<SourceSection, Object, QIRNode>> SLToQIRType = context -> (src, value) -> {
        if (value instanceof BigInteger)
            return new QIRBigNumber(src, (BigInteger) value);
        if (value instanceof Long)
            return new QIRNumber(src, (Long) value);
        if (value instanceof Double)
            return new QIRDouble(src, (Double) value);
        if (value instanceof Boolean)
            return QIRBoolean.create((Boolean) value);
        if (value instanceof String)
            return new QIRString(src, (String) value);
        if (value instanceof SLNull)
            return QIRNull.getInstance();
        if (value instanceof SLFunction) {
            final SLFunction fun = (SLFunction) value;
            if (fun.getCallTarget() == null || fun.getCallTarget().getRootNode() instanceof SLUndefinedFunctionRootNode)
                return new QIRExternal(src, fun.getName());
            final SLRootNode root = (SLRootNode) fun.getCallTarget().getRootNode();
            final SLLanguage language = root.getLanguage(SLLanguage.class);
            try {
                final QIRNode body = root.getBodyNode().accept(new QIRTranslateVisitor(language, fun.frame));
                final String funName = fun.getName();
                if (body instanceof QIRLambda)
                    return new QIRLambda(src, funName, ((QIRLambda) body).getVar(), ((QIRLambda) body).getBody(), new FrameDescriptor());
                return new QIRLambda(src, funName, null, body, new FrameDescriptor());
            } catch (UnsupportedOperationException e) {
                final String funName = fun.getName().replaceFirst("^#*", "");
                String program = src.getCharacters().toString().replaceFirst("^#*", "");
                if (program.trim().startsWith("function"))
                    program += "function main() { return " + funName + "; }";
                else if (program.trim().startsWith("fun"))
                    program = program.replaceFirst("fun", "function " + funName) + "function main() { return " + funName + "; }";
                else if (program.equals("<unavailable>"))
                    program = funName;
                else
                    program = "function main() { return " + program + "; }";
                return root.getBodyNode().accept(new IsExportableVisitor(language, fun.frame))
                                ? new QIRExportableTruffleNode(src, "SL", QIRInterface::execute, QIRInterface::apply, context.getFunctionRegistry().getFunctionDependencies(fun) + program)
                                : new QIRUnexportableTruffleNode(src, "SL", QIRInterface::execute, QIRInterface::apply, context.getFunctionRegistry().getFunctionDependencies(fun) + program);
            }
        }
        if (value instanceof DynamicObject) {
            DynamicObject o = (DynamicObject) value;
            final Object queryId = o.get("queryId");
            if (queryId != null) { // The object is a query
                final int qid = ((Long) queryId).intValue();
                return normalize(context.queries.get(qid), context.envs.get(qid), context);
            }

            final String tableName = (String) o.get("tableName");
            final String schemaName = (String) o.get("schemaName");
            final String dbName = (String) o.get("dbName");
            final String configFile = (String) o.get("configFile");
            if (tableName != null && dbName != null && configFile != null) // The object is a table
                return new QIRTable(src, new QIRString(null, tableName), new QIRString(null, dbName), new QIRString(null, configFile), new QIRString(null, schemaName));
            // else, the object is considered a tuple
            QIRRecord tuple = QIRRnil.getInstance();
            for (final Object x : o.getShape().getKeyList())
                tuple = new QIRRcons(src, (String) x, QIRInterface.SLToQIRType.apply(context).apply(src, o.get(x)), tuple);
            return tuple;
        }
        throw new SLException("Unsupported value: " + value, null);
    };

    static final QIRNode execute(final String program) {
        // TODO: We should use current engine instead
        final Context context;
        try {
            context = Context.newBuilder("sl").build();
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e.getMessage());
        }
        final org.graalvm.polyglot.Source source;
        try {
            source = org.graalvm.polyglot.Source.newBuilder("sl", program, "mySrc").build();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        try {
            Value result = context.eval(source);
            if (context.getBindings("sl").getMember("main") == null) {
                throw new RuntimeException("No function main() defined in SL source file.");
            }
            return SLToQIRType.apply(SLLanguage.getCurrentContext()).apply(null, result.getReceiver());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } finally {
            context.close();
        }
    }

    static final QIRTruffleNode apply(final QIRTruffleNode fun, final List<QIRNode> args) {
        int argIndex = 1;
        String header = "";
        List<String> applyArgs = new ArrayList<>();

        for (QIRNode arg : args) {
            if (arg instanceof QIRRecord) {
                final String argName = "res" + argIndex;
                header += argName + " = new();";
                for (QIRNode v = arg; v instanceof QIRRcons; v = ((QIRRcons) v).getTail()) {
                    final Object value = QIRToSLType(((QIRRcons) v).getValue());
                    header += argName + "." + ((QIRRcons) v).getId() + " = " + (value instanceof String ? "\"" + value + "\"" : value) + ";";
                }
                applyArgs.add(argName);
            } else
                applyArgs.add(QIRToSLType(arg).toString());
        }
        final String code = fun.getCode().replaceFirst("function main\\(\\) \\{ return ([A-Za-z][A-Za-z0-9]*); \\}",
                        "function main(){ " + header + " return $1(" + applyArgs.stream().collect(Collectors.joining(",")) + "); }");
        return fun instanceof QIRExportableTruffleNode ? new QIRExportableTruffleNode(fun.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, code)
                        : new QIRUnexportableTruffleNode(fun.getSourceSection(), "SL", QIRInterface::execute, QIRInterface::apply, code);
    }
}