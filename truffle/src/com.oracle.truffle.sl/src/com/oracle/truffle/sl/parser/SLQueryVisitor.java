package com.oracle.truffle.sl.parser;

import com.oracle.truffle.api.impl.DefaultLoopNode;
import com.oracle.truffle.sl.SLException;
import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.builtins.*;
import com.oracle.truffle.sl.nodes.SLExpressionNode;
import com.oracle.truffle.sl.nodes.SLListNode;
import com.oracle.truffle.sl.nodes.SLNodeVisitor;
import com.oracle.truffle.sl.nodes.SLStatementNode;
import com.oracle.truffle.sl.nodes.access.*;
import com.oracle.truffle.sl.nodes.call.SLInvokeNode;
import com.oracle.truffle.sl.nodes.controlflow.SLBlockNode;
import com.oracle.truffle.sl.nodes.controlflow.SLBreakNode;
import com.oracle.truffle.sl.nodes.controlflow.SLFunctionBodyNode;
import com.oracle.truffle.sl.nodes.controlflow.SLIfNode;
import com.oracle.truffle.sl.nodes.controlflow.SLReturnNode;
import com.oracle.truffle.sl.nodes.controlflow.SLWhileNode;
import com.oracle.truffle.sl.nodes.controlflow.SLWhileRepeatingNode;
import com.oracle.truffle.sl.nodes.expression.*;
import com.oracle.truffle.sl.nodes.local.*;
import com.oracle.truffle.sl.nodes.query.*;
import com.oracle.truffle.sl.qirinterface.QIRTranslateVisitor;
import com.oracle.truffle.sl.runtime.SLContext;

import qir.ast.QIRNode;

/**
 * This visitor detects queries in a SL function and calls {@link QIRTranslateVisitor} to translate
 * them into {@link QIRNode}.
 */
public final class SLQueryVisitor implements SLNodeVisitor<SLStatementNode> {
    private final SLLanguage language;
    private final QIRTranslateVisitor qirVisitor;

    public SLQueryVisitor(final SLLanguage language) {
        this.language = language;
        this.qirVisitor = new QIRTranslateVisitor(language);
    }

    @Override
    public final SLStatementNode visit(final SLFunctionBodyNode body) {
        final SLStatementNode res = new SLFunctionBodyNode(body.getBodyNode().accept(this));
        res.setSourceSection(body.getSourceCharIndex(), body.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLReturnNode ret) {
        final SLStatementNode res = new SLReturnNode(ret.getValueNode() != null ? (SLExpressionNode) ret.getValueNode().accept(this) : null);
        res.setSourceSection(ret.getSourceCharIndex(), ret.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLIfNode ifstmt) {
        final SLStatementNode res = new SLIfNode((SLExpressionNode) ifstmt.getConditionNode().accept(this), ifstmt.getThenPartNode().accept(this),
                        ifstmt.getElsePartNode() != null ? ifstmt.getElsePartNode().accept(this) : null);
        res.setSourceSection(ifstmt.getSourceCharIndex(), ifstmt.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLWhileNode whilestmt) {
        final SLWhileRepeatingNode internNode = (SLWhileRepeatingNode) ((DefaultLoopNode) whilestmt.getLoopNode()).getRepeatingNode();
        final SLStatementNode res = new SLWhileNode((SLExpressionNode) internNode.getConditionNode().accept(this), internNode.getBodyNode().accept(this));
        res.setSourceSection(whilestmt.getSourceCharIndex(), whilestmt.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLBlockNode block) {
        final SLStatementNode[] children = block.getBodyNodes();

        for (int i = 0; i < children.length; i++)
            children[i] = children[i].accept(this);
        return block;
    }

    @Override
    public final SLStatementNode visit(final SLParenExpressionNode e) {
        if (e.getExpression() == null)
            return e;
        final SLStatementNode res = new SLParenExpressionNode((SLExpressionNode) e.getExpression().accept(this));
        res.setSourceSection(e.getSourceCharIndex(), e.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLInvokeNode call) {
        final SLExpressionNode[] args = call.getArgumentNodes();

        for (int i = 0; i < args.length; ++i)
            args[i] = (SLExpressionNode) args[i].accept(this);
        return call;
    }

    @Override
    public final SLStatementNode visit(final SLFunctionLiteralNode fun) {
        return fun;
    }

    @Override
    public final SLReadArgumentNode visit(final SLReadArgumentNode var) {
        return var;
    }

    @Override
    public final SLStatementNode visit(final SLReadLocalVariableNode var) {
        return var;
    }

    @Override
    public final SLStatementNode visit(final SLWriteLocalVariableNode var) {
        final SLStatementNode res = SLWriteLocalVariableNodeGen.create((SLExpressionNode) var.getValueNode().accept(this), var.getSlot());
        res.setSourceSection(var.getSourceCharIndex(), var.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLReadPropertyNode dot) {
        final SLStatementNode res = SLReadPropertyNodeGen.create((SLExpressionNode) dot.getReceiverNode().accept(this), (SLExpressionNode) dot.getNameNode().accept(this));
        res.setSourceSection(dot.getSourceCharIndex(), dot.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLWritePropertyNode writeDot) {
        final SLStatementNode res = SLWritePropertyNodeGen.create((SLExpressionNode) writeDot.getReceiverNode().accept(this), (SLExpressionNode) writeDot.getNameNode().accept(this),
                        (SLExpressionNode) writeDot.getValueNode().accept(this));
        res.setSourceSection(writeDot.getSourceCharIndex(), writeDot.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLListNode list) {
        for (int i = 0; i < list.elements.length; i++)
            list.elements[i] = (SLExpressionNode) list.elements[i].accept(this);
        return list;

    }

    @Override
    public final SLStatementNode visit(final SLAddNode add) {
        final SLStatementNode res = SLAddNodeGen.create((SLExpressionNode) add.getLeftNode().accept(this), (SLExpressionNode) add.getRightNode().accept(this));
        res.setSourceSection(add.getSourceCharIndex(), add.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLSubNode sub) {
        final SLStatementNode res = SLSubNodeGen.create((SLExpressionNode) sub.getLeftNode().accept(this), (SLExpressionNode) sub.getRightNode().accept(this));
        res.setSourceSection(sub.getSourceCharIndex(), sub.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLMulNode mul) {
        final SLStatementNode res = SLMulNodeGen.create((SLExpressionNode) mul.getLeftNode().accept(this), (SLExpressionNode) mul.getRightNode().accept(this));
        res.setSourceSection(mul.getSourceCharIndex(), mul.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLDivNode div) {
        final SLStatementNode res = SLDivNodeGen.create((SLExpressionNode) div.getLeftNode().accept(this), (SLExpressionNode) div.getRightNode().accept(this));
        res.setSourceSection(div.getSourceCharIndex(), div.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLModNode mod) {
        final SLStatementNode res = SLModNodeGen.create((SLExpressionNode) mod.getLeftNode().accept(this), (SLExpressionNode) mod.getRightNode().accept(this));
        res.setSourceSection(mod.getSourceCharIndex(), mod.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLEqualNode eq) {
        final SLStatementNode res = SLEqualNodeGen.create((SLExpressionNode) eq.getLeftNode().accept(this), (SLExpressionNode) eq.getRightNode().accept(this));
        res.setSourceSection(eq.getSourceCharIndex(), eq.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLogicalAndNode and) {
        final SLStatementNode res = new SLLogicalAndNode((SLExpressionNode) and.getLeft().accept(this), (SLExpressionNode) and.getRight().accept(this));
        res.setSourceSection(and.getSourceCharIndex(), and.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLogicalOrNode or) {
        final SLStatementNode res = new SLLogicalOrNode((SLExpressionNode) or.getLeft().accept(this), (SLExpressionNode) or.getRight().accept(this));
        res.setSourceSection(or.getSourceCharIndex(), or.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLessOrEqualNode leq) {
        final SLStatementNode res = SLLessOrEqualNodeGen.create((SLExpressionNode) leq.getLeftNode().accept(this), (SLExpressionNode) leq.getRightNode().accept(this));
        res.setSourceSection(leq.getSourceCharIndex(), leq.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLessThanNode lt) {
        final SLStatementNode res = SLLessThanNodeGen.create((SLExpressionNode) lt.getLeftNode().accept(this), (SLExpressionNode) lt.getRightNode().accept(this));
        res.setSourceSection(lt.getSourceCharIndex(), lt.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLogicalNotNode not) {
        final SLStatementNode res = SLLogicalNotNodeGen.create((SLExpressionNode) not.getValueNode().accept(this));
        res.setSourceSection(not.getSourceCharIndex(), not.getSourceLength());
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLQIRWrapperNode qir) {
        return qir;
    }

    @Override
    public final SLStatementNode visit(final SLSelectNode select) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, select.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLFromNode from) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, from.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLWhereNode where) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, where.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLGroupNode group) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, group.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLOrderNode order) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, order.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLJoinNode join) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, join.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLeftJoinNode join) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, join.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLRightJoinNode join) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, join.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLLimitNode limit) {
        final SLContext context = language.getContextReference().get();
        final SLQIRWrapperNode res = new SLQIRWrapperNode(context);
        context.queries.set(res.id, limit.accept(qirVisitor));
        return res;
    }

    @Override
    public final SLStatementNode visit(final SLBreakNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLDefineFunctionBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLEvalBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLGetSizeBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLHasSizeBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLHelloEqualsWorldBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLImportBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLIsExecutableBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLIsNullBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLNanoTimeBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLNewObjectBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLNextResQueryBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLPrintlnBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLReadlnBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLStackTraceBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLTableBuiltin x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLBooleanNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLStringLiteralNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLLongLiteralNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLDoubleLiteralNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLBigIntegerLiteralNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLNullNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLUnboxNode x) {
        return x;
    }

    @Override
    public final SLStatementNode visit(final SLStatementNode stmt) {
        throw new SLException("Unknown statement: " + stmt.getClass(), stmt);
    }
}