package com.oracle.truffle.sl.nodes;

import com.oracle.truffle.sl.SLException;
import com.oracle.truffle.sl.builtins.*;
import com.oracle.truffle.sl.nodes.access.*;
import com.oracle.truffle.sl.nodes.call.*;
import com.oracle.truffle.sl.nodes.controlflow.*;
import com.oracle.truffle.sl.nodes.expression.*;
import com.oracle.truffle.sl.nodes.local.*;
import com.oracle.truffle.sl.nodes.query.*;

/**
 * An interface for visitors for SL nodes.
 */
public interface SLNodeVisitor<T> {
    public abstract T visit(final SLFunctionBodyNode fBody);

    public abstract T visit(final SLReturnNode ret);

    public abstract T visit(final SLIfNode ifNode);

    public abstract T visit(final SLWhileNode whileNode);

    public abstract T visit(final SLBlockNode block);

    public abstract T visit(final SLParenExpressionNode e);

    public abstract T visit(final SLInvokeNode block);

    public abstract T visit(final SLFunctionLiteralNode fun);

    public abstract T visit(final SLReadArgumentNode var);

    public abstract T visit(final SLReadLocalVariableNode var);

    public abstract T visit(final SLWriteLocalVariableNode var);

    public abstract T visit(final SLReadPropertyNode dot);

    public abstract T visit(final SLWritePropertyNode dot);

    public abstract T visit(final SLListNode list);

    public abstract T visit(final SLAddNode add);

    public abstract T visit(final SLSubNode sub);

    public abstract T visit(final SLMulNode mul);

    public abstract T visit(final SLDivNode div);

    public abstract T visit(final SLModNode mod);

    public abstract T visit(final SLEqualNode eq);

    public abstract T visit(final SLLogicalAndNode and);

    public abstract T visit(final SLLogicalOrNode or);

    public abstract T visit(final SLLessOrEqualNode leq);

    public abstract T visit(final SLLessThanNode lt);

    public abstract T visit(final SLLogicalNotNode not);

    public abstract T visit(final SLQIRWrapperNode qir);

    public abstract T visit(final SLSelectNode select);

    public abstract T visit(final SLFromNode from);

    public abstract T visit(final SLWhereNode where);

    public abstract T visit(final SLGroupNode group);

    public abstract T visit(final SLOrderNode order);

    public abstract T visit(final SLJoinNode join);

    public abstract T visit(final SLLeftJoinNode join);

    public abstract T visit(final SLRightJoinNode join);

    public abstract T visit(final SLLimitNode limit);

    public abstract T visit(final SLBreakNode x);

    public abstract T visit(final SLDefineFunctionBuiltin x);

    public abstract T visit(final SLEvalBuiltin x);

    public abstract T visit(final SLGetSizeBuiltin x);

    public abstract T visit(final SLHasSizeBuiltin x);

    public abstract T visit(final SLHelloEqualsWorldBuiltin x);

    public abstract T visit(final SLImportBuiltin x);

    public abstract T visit(final SLIsExecutableBuiltin x);

    public abstract T visit(final SLIsNullBuiltin x);

    public abstract T visit(final SLNanoTimeBuiltin x);

    public abstract T visit(final SLNewObjectBuiltin x);

    public abstract T visit(final SLNextResQueryBuiltin x);

    public abstract T visit(final SLPrintlnBuiltin x);

    public abstract T visit(final SLReadlnBuiltin x);

    public abstract T visit(final SLStackTraceBuiltin x);

    public abstract T visit(final SLTableBuiltin x);

    public abstract T visit(final SLBooleanNode b);

    public abstract T visit(final SLStringLiteralNode str);

    public abstract T visit(final SLLongLiteralNode num);

    public abstract T visit(final SLDoubleLiteralNode num);

    public abstract T visit(final SLBigIntegerLiteralNode num);

    public abstract T visit(final SLNullNode nul);

    public abstract T visit(final SLUnboxNode nul);

    public default T visit(final SLStatementNode stmt) {
        throw new SLException("Internal error: unknown node to visit: " + stmt.getClass() + ".", stmt);
    }
}