package com.oracle.truffle.sl.qirinterface;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotKind;
import com.oracle.truffle.api.frame.FrameSlotTypeException;
import com.oracle.truffle.api.frame.MaterializedFrame;
import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.builtins.SLDefineFunctionBuiltin;
import com.oracle.truffle.sl.builtins.SLEvalBuiltin;
import com.oracle.truffle.sl.builtins.SLGetSizeBuiltin;
import com.oracle.truffle.sl.builtins.SLHasSizeBuiltin;
import com.oracle.truffle.sl.builtins.SLHelloEqualsWorldBuiltin;
import com.oracle.truffle.sl.builtins.SLImportBuiltin;
import com.oracle.truffle.sl.builtins.SLIsExecutableBuiltin;
import com.oracle.truffle.sl.builtins.SLIsNullBuiltin;
import com.oracle.truffle.sl.builtins.SLNanoTimeBuiltin;
import com.oracle.truffle.sl.builtins.SLNewObjectBuiltin;
import com.oracle.truffle.sl.builtins.SLNextResQueryBuiltin;
import com.oracle.truffle.sl.builtins.SLPrintlnBuiltin;
import com.oracle.truffle.sl.builtins.SLReadlnBuiltin;
import com.oracle.truffle.sl.builtins.SLStackTraceBuiltin;
import com.oracle.truffle.sl.builtins.SLTableBuiltin;
import com.oracle.truffle.sl.nodes.SLExpressionNode;
import com.oracle.truffle.sl.nodes.SLListNode;
import com.oracle.truffle.sl.nodes.SLNodeVisitor;
import com.oracle.truffle.sl.nodes.SLRootNode;
import com.oracle.truffle.sl.nodes.SLStatementNode;
import com.oracle.truffle.sl.nodes.SLUndefinedFunctionRootNode;
import com.oracle.truffle.sl.nodes.access.SLReadPropertyNode;
import com.oracle.truffle.sl.nodes.access.SLWritePropertyNode;
import com.oracle.truffle.sl.nodes.call.SLInvokeNode;
import com.oracle.truffle.sl.nodes.controlflow.SLBlockNode;
import com.oracle.truffle.sl.nodes.controlflow.SLBreakNode;
import com.oracle.truffle.sl.nodes.controlflow.SLFunctionBodyNode;
import com.oracle.truffle.sl.nodes.controlflow.SLIfNode;
import com.oracle.truffle.sl.nodes.controlflow.SLReturnNode;
import com.oracle.truffle.sl.nodes.controlflow.SLWhileNode;
import com.oracle.truffle.sl.nodes.controlflow.SLWhileRepeatingNode;
import com.oracle.truffle.sl.nodes.expression.SLAddNode;
import com.oracle.truffle.sl.nodes.expression.SLBigIntegerLiteralNode;
import com.oracle.truffle.sl.nodes.expression.SLBooleanNode;
import com.oracle.truffle.sl.nodes.expression.SLDivNode;
import com.oracle.truffle.sl.nodes.expression.SLDoubleLiteralNode;
import com.oracle.truffle.sl.nodes.expression.SLEqualNode;
import com.oracle.truffle.sl.nodes.expression.SLFunctionLiteralNode;
import com.oracle.truffle.sl.nodes.expression.SLLessOrEqualNode;
import com.oracle.truffle.sl.nodes.expression.SLLessThanNode;
import com.oracle.truffle.sl.nodes.expression.SLLogicalAndNode;
import com.oracle.truffle.sl.nodes.expression.SLLogicalNotNode;
import com.oracle.truffle.sl.nodes.expression.SLLogicalOrNode;
import com.oracle.truffle.sl.nodes.expression.SLLongLiteralNode;
import com.oracle.truffle.sl.nodes.expression.SLModNode;
import com.oracle.truffle.sl.nodes.expression.SLMulNode;
import com.oracle.truffle.sl.nodes.expression.SLNullNode;
import com.oracle.truffle.sl.nodes.expression.SLParenExpressionNode;
import com.oracle.truffle.sl.nodes.expression.SLStringLiteralNode;
import com.oracle.truffle.sl.nodes.expression.SLSubNode;
import com.oracle.truffle.sl.nodes.expression.SLUnboxNode;
import com.oracle.truffle.sl.nodes.local.SLReadArgumentNode;
import com.oracle.truffle.sl.nodes.local.SLReadLocalVariableNode;
import com.oracle.truffle.sl.nodes.local.SLWriteLocalVariableNode;
import com.oracle.truffle.sl.nodes.query.SLFromNode;
import com.oracle.truffle.sl.nodes.query.SLGroupNode;
import com.oracle.truffle.sl.nodes.query.SLJoinNode;
import com.oracle.truffle.sl.nodes.query.SLLeftJoinNode;
import com.oracle.truffle.sl.nodes.query.SLLimitNode;
import com.oracle.truffle.sl.nodes.query.SLOrderNode;
import com.oracle.truffle.sl.nodes.query.SLQIRWrapperNode;
import com.oracle.truffle.sl.nodes.query.SLRightJoinNode;
import com.oracle.truffle.sl.nodes.query.SLSelectNode;
import com.oracle.truffle.sl.nodes.query.SLWhereNode;
import com.oracle.truffle.sl.runtime.SLFunction;

public class IsExportableVisitor implements SLNodeVisitor<Boolean> {
    private final SLLanguage language;
    private final MaterializedFrame frame;

    public IsExportableVisitor(final SLLanguage language, final MaterializedFrame frame) {
        this.language = language;
        this.frame = frame;
    }

    @Override
    public final Boolean visit(final SLFunctionBodyNode fBody) {
        return fBody.getBodyNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLReturnNode ret) {
        return ret.getValueNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLIfNode ifNode) {
        return ifNode.getConditionNode().accept(this) && ifNode.getThenPartNode().accept(this) && ifNode.getElsePartNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLWhileNode whileNode) {
        final SLWhileRepeatingNode repeating = (SLWhileRepeatingNode) whileNode.getLoopNode().getRepeatingNode();
        return repeating.getConditionNode().accept(this) && repeating.getBodyNode().accept(this);
    }

    /**
     * Translation of a sequence of statements. Note: This is also where the STRAD-ASSIGN rules are
     * handled.
     */
    @Override
    public final Boolean visit(final SLBlockNode block) {
        final SLStatementNode[] children = block.getBodyNodes();

        for (int i = 0; i < children.length; i++)
            if (!children[i].accept(this))
                return false;
        return true;
    }

    @Override
    public final Boolean visit(final SLParenExpressionNode e) {
        return e.getExpression().accept(this);
    }

    @Override
    public final Boolean visit(final SLInvokeNode call) {
        if (!call.getFunctionNode().accept(this))
            return false;

        for (final SLExpressionNode arg : call.getArgumentNodes())
            if (!arg.accept(this))
                return false;
        return true;
    }

    private final Boolean handleFunction(final SLFunction f) {
        final RootCallTarget target = f.getCallTarget();
        if (target == null || target.getRootNode() instanceof SLUndefinedFunctionRootNode)
            return false;
        return ((SLRootNode) target.getRootNode()).getBodyNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLFunctionLiteralNode fun) {
        return handleFunction(language.getContextReference().get().getFunctionRegistry().lookup(fun.getFunctionName(), false));
    }

    @Override
    public final Boolean visit(final SLReadArgumentNode var) {
        return true;
    }

    @Override
    public final Boolean visit(final SLReadLocalVariableNode var) {
        final FrameSlot slot = var.getSlot();

        if (frame == null)
            return true;
        if (frame.getFrameDescriptor().getFrameSlotKind(slot) == FrameSlotKind.Illegal)
            return false;
        try {
            final Object value = frame.getObject(var.getSlot());

            if (value instanceof SLFunction)
                return handleFunction((SLFunction) value);
            return true;
        } catch (FrameSlotTypeException e) {
            return false;
        }
    }

    @Override
    public final Boolean visit(final SLWriteLocalVariableNode var) {
        return var.getValueNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLReadPropertyNode dot) {
        return dot.getReceiverNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLWritePropertyNode writeDot) {
        return writeDot.getReceiverNode().accept(this) && writeDot.getValueNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLListNode list) {
        for (SLExpressionNode e : list.elements)
            if (!e.accept(this))
                return false;
        return true;
    }

    @Override
    public final Boolean visit(final SLAddNode add) {
        return add.getLeftNode().accept(this) && add.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLSubNode sub) {
        return sub.getLeftNode().accept(this) && sub.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLMulNode mul) {
        return mul.getLeftNode().accept(this) && mul.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLDivNode div) {
        return div.getLeftNode().accept(this) && div.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLModNode mod) {
        return mod.getLeftNode().accept(this) && mod.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLEqualNode eq) {
        return eq.getLeftNode().accept(this) && eq.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLLogicalAndNode and) {
        return and.getLeft().accept(this) && and.getRight().accept(this);
    }

    @Override
    public final Boolean visit(final SLLogicalOrNode or) {
        return or.getLeft().accept(this) && or.getRight().accept(this);
    }

    @Override
    public final Boolean visit(final SLLessOrEqualNode leq) {
        return leq.getLeftNode().accept(this) && leq.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLLessThanNode lt) {
        return lt.getLeftNode().accept(this) && lt.getRightNode().accept(this);
    }

    @Override
    public final Boolean visit(final SLLogicalNotNode not) {
        return not.getValueNode().accept(this);
    }

    /**
     * Translation of a subquery.
     */
    @Override
    public final Boolean visit(final SLQIRWrapperNode query) {
        return true;
    }

    @Override
    public final Boolean visit(final SLSelectNode select) {
        return select.formatter.accept(this) && select.query.accept(this);
    }

    @Override
    public final Boolean visit(final SLFromNode from) {
        return from.getTable().accept(this);
    }

    @Override
    public final Boolean visit(final SLWhereNode where) {
        return where.getFilter().accept(this) && where.getQuery().accept(this);
    }

    @Override
    public final Boolean visit(final SLGroupNode group) {
        return group.getGroup().accept(this) && group.getQuery().accept(this);
    }

    @Override
    public final Boolean visit(final SLOrderNode order) {
        return order.getOrder().accept(this) && order.getIsAscending().accept(this) && order.getQuery().accept(this);
    }

    @Override
    public final Boolean visit(final SLJoinNode join) {
        return join.getFilter().accept(this) && join.getLeft().accept(this) && join.getRight().accept(this);
    }

    @Override
    public final Boolean visit(final SLLeftJoinNode join) {
        return join.getFilter().accept(this) && join.getLeft().accept(this) && join.getRight().accept(this);
    }

    @Override
    public final Boolean visit(final SLRightJoinNode join) {
        return join.getFilter().accept(this) && join.getLeft().accept(this) && join.getRight().accept(this);
    }

    @Override
    public final Boolean visit(final SLLimitNode limit) {
        return limit.getLimit().accept(this) && limit.getQuery().accept(this);
    }

    @Override
    public final Boolean visit(final SLBreakNode x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLDefineFunctionBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLEvalBuiltin x) {
        return false;
    }

    @Override
    public final Boolean visit(final SLGetSizeBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLHasSizeBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLHelloEqualsWorldBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLImportBuiltin x) {
        return false;
    }

    @Override
    public final Boolean visit(final SLIsExecutableBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLIsNullBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLNanoTimeBuiltin x) {
        return true;
    }

    @Override
    public final Boolean visit(final SLNewObjectBuiltin tnil) {
        return true;
    }

    @Override
    public final Boolean visit(final SLNextResQueryBuiltin next) {
        return true;
    }

    @Override
    public final Boolean visit(final SLPrintlnBuiltin x) {
        return false;
    }

    @Override
    public final Boolean visit(final SLReadlnBuiltin x) {
        return false;
    }

    @Override
    public final Boolean visit(final SLStackTraceBuiltin x) {
        return false;
    }

    // TODO: This is not how to handle a table call properly
    @Override
    public final Boolean visit(final SLTableBuiltin table) {
        return true;
    }

    @Override
    public final Boolean visit(final SLBooleanNode b) {
        return true;
    }

    @Override
    public final Boolean visit(final SLStringLiteralNode s) {
        return true;
    }

    @Override
    public final Boolean visit(final SLLongLiteralNode num) {
        return true;
    }

    @Override
    public final Boolean visit(final SLDoubleLiteralNode num) {
        return true;
    }

    @Override
    public final Boolean visit(final SLBigIntegerLiteralNode num) {
        return true;
    }

    @Override
    public final Boolean visit(final SLNullNode s) {
        return true;
    }

    @Override
    public final Boolean visit(final SLUnboxNode stmt) {
        return true;
    }

    @Override
    public final Boolean visit(final SLStatementNode stmt) {
        throw new UnsupportedOperationException("Cannot translate to QIR: " + stmt.getSourceSection() + System.lineSeparator() + stmt.getClass().getName());
    }
}
