package com.oracle.truffle.sl.runtime;

import java.io.Serializable;

public class SLValueFunction implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String name;
    private final String code;
    private final String dependencies;

    public SLValueFunction(final String name, final String code, final String dependencies) {
        this.name = name;
        this.code = code;
        this.dependencies = dependencies;
    }

    public final String getName() {
        return name;
    }

    public final String getCode() {
        return code;
    }

    public final String getDependencies() {
        return dependencies;
    }
}