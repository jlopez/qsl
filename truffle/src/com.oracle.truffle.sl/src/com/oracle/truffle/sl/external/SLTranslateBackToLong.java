package com.oracle.truffle.sl.external;

import org.apache.hadoop.hive.ql.exec.UDF;

import com.oracle.truffle.sl.SLLanguage;
import com.oracle.truffle.sl.runtime.SLValue;

public class SLTranslateBackToLong extends UDF {
    public static Long evaluate(final SLValue v) {
        return SLLanguage.translateBackToLong(v);
    }
}
