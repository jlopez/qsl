/*
 * Copyright (c) 2012, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.sl.nodes.query;

import com.oracle.truffle.api.CompilerDirectives.*;
import com.oracle.truffle.api.frame.*;
import com.oracle.truffle.api.nodes.*;
import com.oracle.truffle.api.object.*;
import com.oracle.truffle.sl.nodes.*;
import com.oracle.truffle.sl.runtime.*;

@NodeInfo(shortName = "query", description = "The node representing a query")
public final class SLQIRWrapperNode extends SLExpressionNode {
    private final SLContext context;
    // The unique identifier of the query
    public final int id;

    public SLQIRWrapperNode(final SLContext context) {
        this.context = context;
        this.id = context.createFreshQuery();
    }

    @Override
    public DynamicObject executeGeneric(VirtualFrame frame) {
        final int instanceId = context.envs.get(id) == null ? id : context.createFreshQueryFrom(id);
        context.envs.set(instanceId, frame);
        return createQuery(instanceId);
    }

    @TruffleBoundary
    private final DynamicObject createQuery(int instanceId) {
        final DynamicObject res = context.createObject();
        res.define("queryId", Long.valueOf(instanceId));
        return res;
    }

    @Override
    public <T> T accept(SLNodeVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
